﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LearnSoftware.Models.Information
{
    public class ForumCategory
    {
        public ForumCategory()
        {
            Forums = new List<Forum>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<Forum> Forums { get; set; }
    }
}
