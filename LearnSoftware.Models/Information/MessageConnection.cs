﻿using System;
using System.ComponentModel.DataAnnotations;
using LearnSoftware.Areas.Identity.Data;

namespace LearnSoftware.Models.Information
{
    public class MessageConnection
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public LSIdentityUser Participant1 { get; set; }

        [Required]
        public LSIdentityUser Participant2 { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }
    }
}
