﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LearnSoftware.Services.Interfaces
{
    public interface ILoadBalancingService
    {
        void UpdateMachine(string machineId);

        int GetWaitTimeMS(int itemCount);
    }
}
