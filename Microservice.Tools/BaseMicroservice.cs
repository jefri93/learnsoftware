﻿using System;
using System.Threading.Tasks;
using LoggerLite;
using Microservice.Tools.Interfaces;

namespace Microservice.Tools
{
    public abstract class BaseMicroservice : IMicroservice
    {
        public BaseMicroservice(ILogger logger, IMSConfiguration configuration)
        {
            this.Logger = logger;
            this.Configuration = configuration;
        }

        protected IMSConfiguration Configuration { get; }

        protected ILogger Logger { get; }

        public abstract void Run();
    }
}
