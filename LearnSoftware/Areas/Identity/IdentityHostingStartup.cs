﻿using System;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Data;
using LearnSoftware.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(LearnSoftware.Areas.Identity.IdentityHostingStartup))]
namespace LearnSoftware.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<LSIdentityContext>(options =>

                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("LearnSoftware")));

                services.AddIdentity<LSIdentityUser, IdentityRole>()
                    .AddDefaultUI()
                    .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<LSIdentityContext>();
            });
        }
    }
}