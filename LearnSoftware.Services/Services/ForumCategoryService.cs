﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using LearnSoftware.Models.Information;
using LearnSoftware.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LearnSoftware.Services
{
    public class ForumCategoryService : IForumCategoryService
    {
        private LSIdentityContext context;
        private IPageManager pageService;

        public ForumCategoryService(LSIdentityContext context, IPageManager pageService)
        {
            this.context = context;
            this.pageService = pageService;
        }

        public ForumCategory GetForumCategory(int forumCategoryId)
        {
            return this.context.ForumCategories.FirstOrDefault(c => c.Id == forumCategoryId);
        }

        public bool HasForumCategory(int forumCategoryId)
        {
            return this.context.ForumCategories.Any(c => c.Id == forumCategoryId);
        }

        public bool HasForumCategory(string name)
        {
            return this.context.ForumCategories.Any(c => c.Name == name);
        }

        public IEnumerable<ForumCategory> GetForumCategories()
        {
            return this.context.ForumCategories.ToList();
        }

        public string ValidateCategoryName(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                return "Category name cannot be empty";
            }

            bool hasCategory = this.HasForumCategory(name);

            if (hasCategory)
            {
                return "Category already exists.";
            }

            return String.Empty;
        }

        public void CreateForumCategory(string name)
        {
            ForumCategory category = new ForumCategory()
            {
                Name = name
            };

            context.ForumCategories.Add(category);
            context.SaveChanges();
        }
    }
}
