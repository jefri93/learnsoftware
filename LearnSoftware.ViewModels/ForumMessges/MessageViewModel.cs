﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LearnSoftware.ViewModels.Messges
{
    public class MessageViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        public int CurrentPage { get; set; }
    }
}
