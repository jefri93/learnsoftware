﻿using System;
using System.Collections.Generic;
using LearnSoftware.Models.Learn;

namespace LearnSoftware.Services.Interfaces
{
    public interface ILectureService
    {
        IEnumerable<Lecture> GetLectures(int courseId);

        Lecture GetLecture(int id);

        int CreateLecture(Course course, string textExplnation, string videoUrl, string name, int videoLengthInMinutes);

        void UpdateLecture(Lecture lecture, string textExplnation, string videoUrl, string name, int videoLengthInMinutes);

        bool HasLecture(int lectureId);
    }
}
