﻿using System;
using System.Collections.Generic;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models.Information;

namespace LearnSoftware.Services.Interfaces
{
    public interface IForumMessageService
    {
        ForumMessage GetForumMessage(int forumMessageId);

        IEnumerable<ForumMessage> GetForumMessages(int? forumId = null, int? page = null);

        int GetCount(int? forumId = null);

        void UpdateMessage(ForumMessage message, string messageText);

        void AddMessage(Forum forum, LSIdentityUser user, string comment);
    }
}
