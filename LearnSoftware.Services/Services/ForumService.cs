﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using LearnSoftware.Models.Information;
using LearnSoftware.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LearnSoftware.Services
{
    public class ForumService : IForumService
    {
        private LSIdentityContext context;
        private IPageManager pageService;

        public ForumService(LSIdentityContext context, IPageManager pageService)
        {
            this.context = context;
            this.pageService = pageService;
        }

        public IEnumerable<Forum> GetForums(int? forumCategoryId = null, int? page = null, string search = null, string commenterUserId = null)
        {
            if(page < 0)
            {
                return new List<Forum>();
            }

            IEnumerable<Forum> forums = this.context.Forums
                .Include(fc => fc.ForumCategory)
                .Include(f => f.Creator)
                .Include(fc => fc.Messages)
                .Where(fc => fc.ForumCategory.Id == forumCategoryId || forumCategoryId == null);

            if(!String.IsNullOrEmpty(commenterUserId))
            {
                forums = forums.Where(f => f.Messages.Any(m => m.UserId == commenterUserId));
            }

            if (!String.IsNullOrEmpty(search))
            {
                search = search.ToLower();
                forums = forums.Where(f => f.Name.ToLower().Contains(search) || search.Contains(f.Name.ToLower()));
            }

            if (page != null)
            {
                var skip = pageService.GetSkipCount((int)page);
                var take = pageService.ElementsPerPage;

                forums = forums.OrderByDescending(f => f.CreationDate)
                    .Skip(skip)
                    .Take(take);
            }

            return forums.ToList();
        }

        public int GetCount(int? forumCategoryId = null)
        {
            if (forumCategoryId != null)
            {
                return context.Forums.Count(f => f.ForumCategory.Id == forumCategoryId);
            }

            return context.Forums.Count();
        }

        public Forum GetForum(int id)
        {
            return this.context.Forums.Include(f => f.Creator).FirstOrDefault(f => f.Id == id);
        }

        public int AddForum(LSIdentityUser user, ForumCategory forumCategory, string forumName, string firstComment)
        {
            Forum forum = new Forum()
            {
                CreationDate = DateTime.UtcNow,
                UpdateDate = DateTime.UtcNow,
                Creator = user,
                IsActive = true,
                ForumCategory = forumCategory,
                Name = forumName
            };

            forum.Messages.Add(new ForumMessage()
            {
                CreationDate = DateTime.UtcNow,
                UpdateDate = DateTime.UtcNow,
                Forum = forum,
                Message = firstComment,
                User = user,
                UserId = user.Id
            });

            this.context.Forums.Add(forum);
            this.context.SaveChanges();

            return forum.Id;
        }

        public void DeactivateForum(Forum forum)
        {
            forum.IsActive = false;
            context.Update(forum);
            context.SaveChanges();
        }
    }
}
