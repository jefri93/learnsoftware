﻿using System;
using Microsoft.AspNetCore.Http;

namespace LearnSoftware.Services.Interfaces
{
    public interface ISessionManager
    {
		byte[] ObjectToByteArray<T>(T obj);

        T ByteArrayToObject<T>(byte[] arrBytes);

        T GetSessionObject<T>(string key, ISession session);

        void SetSessionObject<T>(string key, T obj, ISession session);
    }
}
