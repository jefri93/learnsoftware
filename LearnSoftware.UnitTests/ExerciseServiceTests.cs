﻿using System;
using System.Collections.Generic;
using System.Linq;
using LearnSoftware.Services;
using LearnSoftware.Services.Interfaces;
using LearnSoftware.ViewModels.Exercises;
using Moq;
using Xunit;

namespace LearnSoftware.UnitTests
{
    public class ExerciseServiceTests
    {
        LectureService lectureService;
        IExerciseService exerciseService;

        public ExerciseServiceTests()
        {
            var context = TestData.GetDataFilledContext();
            exerciseService = new ExerciseService(context, new Mock<PageManager>().Object);
            lectureService = new LectureService(context);
        }

        [Fact]
        public void GetTestResultsTest()
        {
            var exercise = exerciseService.GetExercise(6);
            var testResults = exerciseService.GetTestResults(exercise, "User2");

            Assert.True(1 == testResults.Count());
        }

        [Fact]
        public void CreateExerciseTest()
        {
            var lecture = lectureService.GetLecture(6);
            var id = exerciseService.CreateExercise(lecture, "n", "d", "t", "t1", "", "", new List<TestLinesViewModel>());

            Assert.True(id > 0);
        }

        [Fact]
        public void UpdateExerciseTest()
        {
            var lecture = lectureService.GetLecture(6);
            var id = exerciseService.CreateExercise(lecture, "n", "d", "t", "t1", "", "", new List<TestLinesViewModel>());
            var exercise = exerciseService.GetExercise(id);
            exerciseService.UpdateExercise(exercise, "Updated Name", "d", "t", "t1", "", "", new List<TestLinesViewModel>());

            Assert.True(exercise.Name == "Updated Name");
        }
    }
}
