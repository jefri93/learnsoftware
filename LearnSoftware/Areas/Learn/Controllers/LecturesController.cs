﻿using System.Linq;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.ViewModels.Lectures;
using LearnSoftware.Controllers;
using LearnSoftware.Models;
using LearnSoftware.Models.Learn;
using LearnSoftware.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LearnSoftware.Services.Interfaces;
using System;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LearnSoftware.Areas.Learn.Controllers
{
    [Area("Learn")]
    public class LecturesController : Controller
    {
        private ICourseService courseService;
        private ILectureService lectureService;
        private ISubscriptionService subscriptionService;
        private UserManager<LSIdentityUser> userManager;

        public LecturesController(ICourseService courseService, ILectureService lectureService, ISubscriptionService subscriptionService, UserManager<LSIdentityUser> userManager)
        {
            this.courseService = courseService;
            this.lectureService = lectureService;
            this.subscriptionService = subscriptionService;
            this.userManager = userManager;
        }


        // GET: /<controller>/
        [Authorize]
        public IActionResult Lecture(int id)
        {
            var lecture = lectureService.GetLecture((int)id);

            if(lecture == null)
            {
                this.NotFound();
            }

            if (!lecture.IsCourseActive && !this.User.IsInRole("Admin"))
            {
                return this.Unauthorized();
            }

            ViewData["IsUserSubscribed"] = subscriptionService.IsUserSubscribed(this.User.Identity.Name, lecture.CourseId);

            LectureLectureViewModel model = new LectureLectureViewModel()
            {
                Id = lecture.Id,
                Name = lecture.Name,
                CourseType = lecture.Course.CourseType,
                LectureTextExplnation = lecture.LectureTextExplnation,
                LectureVideoUrl = lecture.LectureVideoUrl,
                CourseId = lecture.CourseId,
                Exercises = lecture.Exercices.Select(e => new LectureExerciseViewModel()
                {
                    Id = e.Id,
                    Name = e.Name,
                }).ToList()
            };

            var user = userManager.FindByNameAsync(this.User.Identity.Name).Result;

            if (user == null || lecture?.Course == null)
            {
                return this.NotFound();
            }

            if (!courseService.HasUserCourse(user.Id, lecture.Course.Id)
                && (courseService.IsCourseFree(lecture.Course.Id) 
                || courseService.IsCoursePurchased(lecture.Course.Id, user.Id)))
            {
                courseService.CreateUserCourse(lecture.Course, user);
            }

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("/Lectures/Edit/{lectureId}")]
        public IActionResult Edit(int lectureId)
        {
            var lecture = lectureService.GetLecture(lectureId);

            if (lecture == null)
            {
                return this.NotFound();
            }

            LectureCreateViewModel model = new LectureCreateViewModel()
            {
                Id = lectureId,
                Name = lecture.Name,
                LectureTextExplnation = lecture.LectureTextExplnation,
                LectureVideoUrl = lecture.LectureVideoUrl,
                LectureVideoLengthInMinutes = (int)Math.Floor(new TimeSpan(lecture.VideoLength).TotalMinutes)
            };

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("/Lectures/Edit/{lectureId}")]
        public IActionResult Edit(LectureCreateViewModel model)
        {
            if(!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var lecture = lectureService.GetLecture(model.Id);

            if(lecture == null)
            {
                return this.NotFound();
            }

            lectureService.UpdateLecture(lecture, model.LectureTextExplnation, model.LectureVideoUrl, model.Name, model.LectureVideoLengthInMinutes);

            return this.RedirectToAction("Lecture", "Lectures", new { id = model.Id });
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("/Lectures/Create/{courseId}")]
        public IActionResult Create(int courseId)
        {
            var courseExists = courseService.HasCourse((int)courseId);

            if (!courseExists)
            {
                return this.NotFound();
            }

            LectureCreateViewModel model = new LectureCreateViewModel() { Id = (int)courseId };

            return View(model);
        }

        [HttpPost("/Lectures/Create/{courseId}")]
        [Authorize(Roles = "Admin")]
        public IActionResult Create(LectureCreateViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            Course course = courseService.GetCourse(model.Id);

            if(course == null)
            {
                return this.BadRequest();
            }

            int lectureId = lectureService.CreateLecture(course, model.LectureTextExplnation, model.LectureVideoUrl, model.Name, model.LectureVideoLengthInMinutes);

            return RedirectToAction("Lecture","Lectures", new { id = lectureId });
        }
    }
}
