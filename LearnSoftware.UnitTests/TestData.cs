﻿using System;
using System.Collections.Generic;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using LearnSoftware.Models.Learn;
using Microsoft.EntityFrameworkCore;

namespace LearnSoftware.UnitTests
{
    public static class TestData
    {
        public static LSIdentityContext GetDataFilledContext()
        {
            var builder = new DbContextOptionsBuilder<LSIdentityContext>()
            .UseInMemoryDatabase();

            Course course = new Course()
            {
                IsActive = false,
                Description = "description",
                CourseType = CourseType.Free,
                Name = "Course",
                Id = 6,
                Lectures = new List<Lecture>()
                {
                    new Lecture()
                    {
                        Id = 6,
                        Name = "Lecture",
                        LectureTextExplnation ="Explain",
                        Exercices = new List<Exercise>()
                        {
                            new Exercise()
                            {
                                Description = "Descr",
                                Id = 6,
                                Name = "Exercise",
                                ExampleInput1 = "Test",
                                ExampleOutput1 = "Test 1",
                                ExerciseTests = new List<ExerciseTest>()
                                {
                                    new ExerciseTest()
                                    {
                                        TestExpectedOutputLines = "Test 1",
                                        Id = 6,
                                        TestInputs = new List<TestInputLine>()
                                        {
                                            new TestInputLine()
                                            {
                                                Id = 6,
                                                Text = "Test"
                                            }
                                        },
                                        TestResults = new List<TestResult>()
                                        {
                                            new TestResult()
                                            {
                                                Id = 6,
                                                LinesRead = 1,
                                                Message = null,
                                                Success = true,
                                                WriteCount = 1,
                                                User = new LSIdentityUser()
                                                {
                                                    UserName = "User2",
                                                    Id = "2"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    new Lecture()
                    {
                        Id = 7,
                        Name = "Lecture 2",
                        LectureTextExplnation ="Explain 2"
                    }
                }
            };

            Course course2 = new Course()
            {
                IsActive = false,
                Description = "description 2",
                CourseType = CourseType.Free,
                Name = "Course 2",
                Id = 7
            };

            course2.UsersCourses = new List<UserCourse>()
            {
                new UserCourse()
                {
                    Course = course2,
                    CourseId = course2.Id,
                    User = new LSIdentityUser()
                    {
                        Id = "1",
                        UserName = "User"
                    },
                    UserId = "1"
                }
            };

            var context = new LSIdentityContext(builder.Options);
            context.Courses.Add(course);
            context.Courses.Add(course2);
            context.SaveChanges();

            return context;
        }
    }
}
