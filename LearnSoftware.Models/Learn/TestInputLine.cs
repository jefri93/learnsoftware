﻿using System.ComponentModel.DataAnnotations;

namespace LearnSoftware.Models.Learn
{
    public class TestInputLine
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public ExerciseTest ExerciseTest { get; set; }
    }
}
