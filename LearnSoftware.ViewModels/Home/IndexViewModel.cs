﻿using System;
using System.Collections.Generic;

namespace LearnSoftware.ViewModels.Home
{
    public class IndexViewModel
    {
        public Dictionary<int, string> NewestCourses { get; set; }

        public Dictionary<int, string> ViewedCourses { get; set; }

        public Dictionary<int, string> NewestForums { get; set; }

        public Dictionary<int, string> ParticipatedForums { get; set; }
    }
}
