﻿using System;
namespace LearnSoftware.ViewModels.ForumCategories
{
    public class FocumCategoryIndexForumViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string CreatorUsername { get; set; }

        public string CreatedOn { get; set; }
    }
}
