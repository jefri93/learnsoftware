﻿using System;
namespace LearnSoftware.Services.Interfaces
{
    public interface IPageManager
    {
        int ElementsPerPage { get; }

        int GetMaxPage(int itemCount);

        int GetMinPage(int maxPage);

        int GetSkipCount(int page);
    }
}
