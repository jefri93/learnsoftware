﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using LearnSoftware.Services;
using LearnSoftware.Services.Dtos;
using LearnSoftware.Services.Interfaces;
using LearnSoftware.ViewModels.Home;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LearnSoftware.Controllers
{
    public class HomeController : Controller
    {
        private IForumService forumService;
        private ICourseService courseService;
        private IAdminManager adminManager;
        private UserManager<LSIdentityUser> userManager;
        private SignInManager<LSIdentityUser> signInManager;
        private IExerciseService exerciseService;
        private ILoadBalancingService loadBalancingService;
        private IConfiguration configuration;

        public HomeController(IForumService forumService, ICourseService courseService, UserManager<LSIdentityUser> userManager, SignInManager<LSIdentityUser> signInManager, IAdminManager adminManager, IExerciseService exerciseService, ILoadBalancingService loadBalancingService, IConfiguration configuration)
        {
            this.forumService = forumService;
            this.courseService = courseService;
            this.adminManager = adminManager;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.exerciseService = exerciseService;
            this.loadBalancingService = loadBalancingService;
            this.configuration = configuration;
        }

        private bool AuthenticateApiUser()
        {
            var configUser = configuration.GetValue<string>("AuthUsername");
            var configPass = configuration.GetValue<string>("AuthPassword");

            var actual = Request.Headers["Authorization"].ToString();
            var expected = "Basic " + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes($"{configUser}:{configPass}"));

            return actual == expected;
        }

        public IActionResult Index()
        {
            adminManager.CreateRoles();
            adminManager.CreateDefaultAdmin();

            if (signInManager.IsSignedIn(User))
            {
                var user = this.userManager.FindByNameAsync(this.User.Identity.Name).Result;

                if (user == null)
                {
                    signInManager.SignOutAsync();
                    return this.View();
                }

                var newestCourses = this.courseService.GetCourses(null,this.User.IsInRole("Admin"))
                                        .OrderByDescending(c => c.Id)
                                        .Take(10)
                                        .ToDictionary(k => k.Id, v => v.Name);

                var participatedForums = this.forumService.GetForums(null, 1,null, user.Id)
                                                   .ToDictionary(k => k.Id, v => v.Name);

                var newestForums = this.forumService.GetForums(null, 1)
                                       .ToDictionary(k => k.Id, v => v.Name);

                var viewedCourses = courseService.GetCourses(null, this.User.IsInRole("Admin"))
                                                   .Where(c => c.UsersCourses.Any(uc => uc.UserId == user.Id))
                                                   .OrderByDescending(c => c.Id)
                                                   .Take(10)
                                                   .ToDictionary(k => k.Id, v => v.Name);

                var model = new HomeIndexViewModel()
                {
                    NewestCourses = newestCourses,
                    ViewedCourses = viewedCourses,
                    NewestForums = newestForums,
                    ParticipatedForums = participatedForums
                };

                return this.View("IndexLoggedIn",model);
            }

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [IgnoreAntiforgeryToken]
        public IActionResult GetSleepMiliseconds()
        {
            if (!AuthenticateApiUser())
            {
                return Json("Unauthorized");
            }

            int items = exerciseService.GetTestProcessingCount();
            return Json(loadBalancingService.GetWaitTimeMS(items));
        }

        [IgnoreAntiforgeryToken]
        public IActionResult GetAllUnprocessedTests(string id)
        {
            if (!AuthenticateApiUser() || id == null)
            {
                return Json("Unauthorized");
            }

            loadBalancingService.UpdateMachine(id);
            return Json(exerciseService.GetAllUnprocessedTests(id));
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult UpdateTestResultStatus(UpdateTestResultStatusDto dto)
        {
            if (!AuthenticateApiUser())
            {
                return Json("Unauthorized");
            }

            loadBalancingService.UpdateMachine(dto.MachineId);
            var success = exerciseService.UpdateTestResultStatus(dto);
            return Json(success);
        }
    }
}
