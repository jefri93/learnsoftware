﻿using System;
using Microservice.Tools.Interfaces;

namespace Microservice.Tools
{
    public class MSConfiguration : IMSConfiguration
    {
        public int TimeIntervalMs { get; set; }

        public int Timeout { get; set; }

        public int CodeBlockByteLimit { get; set; }

        public int TestResultProcessingThreadCount { get; set; }

        public string[] ForbiddenKeywords { get; set; }

        public int MaxProcessedCount { get; set; }

        public long MemoryThreshold { get; set; }
    }
}
