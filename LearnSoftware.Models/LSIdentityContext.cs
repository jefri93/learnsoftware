﻿using LearnSoftware.Models.Information;
using LearnSoftware.Models.Learn;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LearnSoftware.Areas.Identity.Data
{
    public class LSIdentityContext : IdentityDbContext<LSIdentityUser>
    {
        public LSIdentityContext(DbContextOptions<LSIdentityContext> options)
            : base(options)
        {
        }

        public DbSet<TestResultTestClass> TestResultsTestClasses { get; set; }

        public DbSet<Subscription> Subscriptions { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Lecture> Lectures { get; set; }

        public DbSet<Exercise> Exercises { get; set; }

        public DbSet<ExerciseTest> ExerciseTests { get; set; }

        public DbSet<TestInputLine> TestInputLines { get; set; }

        public DbSet<TestResult> TestResults { get; set; }

        public DbSet<TestClass> TestClasses { get; set; }

        public DbSet<UserCourse> UsersCourses { get; set; }

        public DbSet<ForumCategory> ForumCategories { get; set; }

        public DbSet<Forum> Forums { get; set; }

        public DbSet<ForumMessage> ForumMessages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            modelBuilder.Entity<Course>().HasMany(e => e.Lectures).WithOne(e => e.Course);
            modelBuilder.Entity<Lecture>().HasMany(e => e.Exercices).WithOne(e => e.Lecture);
            modelBuilder.Entity<Exercise>().HasMany(e => e.ExerciseTests).WithOne(e => e.Exercise);
            modelBuilder.Entity<ExerciseTest>().HasOne(et => et.Exercise).WithMany(e => e.ExerciseTests).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ExerciseTest>().HasMany(e => e.TestInputs).WithOne(e => e.ExerciseTest).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ExerciseTest>().HasMany(e => e.TestResults).WithOne(e => e.ExerciseTest).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TestResult>().HasMany(e => e.TestResultsTestClasses).WithOne(e => e.TestResult).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<LSIdentityUser>().HasMany(u => u.Subscriptions);
            modelBuilder.Entity<ForumCategory>().HasIndex(u => u.Name).IsUnique();
            modelBuilder.Entity<ForumCategory>().HasMany(fc => fc.Forums).WithOne(f => f.ForumCategory);
            modelBuilder.Entity<Forum>().HasMany(f => f.Messages).WithOne(m => m.Forum);
            modelBuilder.Entity<LSIdentityUser>().HasMany(f => f.CreatedForums).WithOne(u => u.Creator);
            modelBuilder.Entity<ForumMessage>().HasOne(fm => fm.User).WithMany(u => u.ForumMessages).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<LSIdentityUser>()
                        .HasIndex(u => u.Email)
                        .IsUnique();

            modelBuilder.Entity<UserCourse>()
                        .HasKey(bc => new { bc.UserId, bc.CourseId });
            modelBuilder.Entity<UserCourse>()
                        .HasOne(bc => bc.User)
                        .WithMany(b => b.ViewedCourses)
                        .HasForeignKey(bc => bc.UserId);
            modelBuilder.Entity<UserCourse>()
                        .HasOne(bc => bc.Course)
                        .WithMany(c => c.UsersCourses)
                        .HasForeignKey(bc => bc.CourseId);

            modelBuilder.Entity<TestResultTestClass>()
                       .HasKey(bc => new { bc.TestClassId, bc.TestResultId });
            modelBuilder.Entity<TestResultTestClass>()
                        .HasOne(bc => bc.TestResult)
                        .WithMany(b => b.TestResultsTestClasses)
                        .HasForeignKey(bc => bc.TestResultId);
            modelBuilder.Entity<TestResultTestClass>()
                        .HasOne(bc => bc.TestClass)
                        .WithMany(c => c.TestResultsTestClasses)
                        .HasForeignKey(bc => bc.TestClassId);

            modelBuilder.Entity<LSIdentityUser>()
                .HasMany(u => u.Subscriptions);
        }


    }
}
