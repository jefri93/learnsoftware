﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Microservice.Tools.Interfaces
{
    internal interface IDataLayer
    {
        int ExecuteNonQueryProcedure(string prcName, Dictionary<string, object> parameters = null);

        DataTable GetTableFromProcedure(string prcName, Dictionary<string, object> parameters = null);

        DataSet GetDataSetFromProcedure(string prcName, Dictionary<string, object> parameters = null);
    }
}
