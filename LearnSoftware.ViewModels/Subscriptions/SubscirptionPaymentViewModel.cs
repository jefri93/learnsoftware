﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LearnSoftware.ViewModels.Subscriptions
{
    public class SubscirptionPaymentViewModel
    {
        [Required]
        public string payment_method_nonce { get; set; }

        [Required]
        public int course_id { get; set; }
    }
}
