﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Teacher.ResultComaprers
{
    public class CompareResult
    {
        public CompareResult(bool success, string json)
        {
            this.Success = success;
            this.Json = json;
        }

        public bool Success { get; }

        public string Json { get; }
    }
}
