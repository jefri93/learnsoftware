﻿using System;
using System.Collections.Generic;
using LearnSoftware.Models;

namespace LearnSoftware.ViewModels.Lectures
{
    public class LectureLectureViewModel
    {
        public LectureLectureViewModel()
        {
            Exercises = new List<LectureExerciseViewModel>();
        }

        public int Id { get; set; }

        public string LectureVideoUrl { get; set; }

        public CourseType CourseType { get; set; }

        public string Name { get; set; }

        public string LectureTextExplnation { get; set; }

        public int CourseId { get; set; }

        public List<LectureExerciseViewModel> Exercises { get; set; }
    }
}
