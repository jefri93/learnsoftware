﻿using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LearnSoftware.Services.Services
{
    public class LoadBalancingService : ILoadBalancingService
    {
        private static DateTime LastTimeProvided = DateTime.UtcNow;
        private ConcurrentDictionary<string, DateTime> machines;
        private LSIdentityContext context;

        public LoadBalancingService(LSIdentityContext context)
        {
            this.context = context;
            machines = new ConcurrentDictionary<string, DateTime>();
        }

        public void UpdateMachine(string machineId)
        {
            machines.AddOrUpdate(machineId, DateTime.UtcNow, (key, oldValue) => DateTime.UtcNow);

            var machinesToRemove = machines.Where(m => m.Value.AddMinutes(2) < DateTime.UtcNow);

            foreach (var machine in machinesToRemove)
            {
                DateTime removedDateTime;
                machines.TryRemove(machine.Key, out removedDateTime);

                //known bug which should not affect it may remove machineId to trtc that was just updated 
                //but the chances are small as the service should be dead.
                var testResultsTestClasses = context.TestResultsTestClasses
                    .Where(trtc => (trtc.MachineId == machine.Key) && trtc.TestResult.Success == null);

                foreach (var trtc in testResultsTestClasses)
                {
                    trtc.MachineId = null;
                    trtc.SentForProcessingAt = null;
                }

                context.SaveChangesAsync();
            }
        }

        public int GetWaitTimeMS(int itemCount)
        {
            int timeInMS = 100;

            if (itemCount == 0 && LastTimeProvided.AddSeconds(15) >= DateTime.UtcNow)
            {
                timeInMS = (int)Math.Round((DateTime.UtcNow - LastTimeProvided).TotalMilliseconds) + 15000;
            }

            LastTimeProvided = DateTime.UtcNow;

            return timeInMS;
        }
    }
}
