﻿using System;
using System.Collections.Generic;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using LearnSoftware.Models.Learn;

namespace LearnSoftware.Services.Interfaces
{
    public interface ICourseService
    {
        void ChangeCourseStatus(Course course, bool? isActive = null);

        IEnumerable<Course> GetCourses(string search = null, bool isAdmin = false);

        IEnumerable<Course> GetCoursesInProgress(string username, string search, bool isAdmin);

        IEnumerable<Course> GetPurchasedCourses(string username, string search, bool isAdmin);

        int CreateCourse(CourseType courseType, string name, string description, decimal price);

        void UpdateCourse(Course course, CourseType courseType, string name, string description, decimal price);

        Course GetCourse(int courseId);

        bool HasCourse(int courseId);

        bool HasUserCourse(string userId, int courseId);

        void CreateUserCourse(Course course, LSIdentityUser user);

        bool IsCourseFree(int courseId);

        bool IsCoursePurchased(int courseId, string username);
    }
}
