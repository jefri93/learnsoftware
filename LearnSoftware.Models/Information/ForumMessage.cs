﻿using System;
using System.ComponentModel.DataAnnotations;
using LearnSoftware.Areas.Identity.Data;

namespace LearnSoftware.Models.Information
{
    public class ForumMessage
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Message { get; set; }

        public string UserId { get; set; }

        [Required]
        public LSIdentityUser User { get; set; }

        [Required]
        public Forum Forum { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime UpdateDate { get; set; }
    }
}
