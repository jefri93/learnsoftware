﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Teacher.ResultComaprers
{
    public interface IResultComparer
    {
        CompareResult Compare(int testResultId);

        CompareResult Compare(List<string> expectedResultLines, List<string> actualResultLines);
    }
}
