﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnSoftware.Migrations
{
    public partial class ChangeForumMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "ForumMessages",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "ForumMessages",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
