﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Microservice.Tools.Helpers
{
    public static class DirectoryHelper
    {
        internal static string GetOutputDirectory()
        {
            string currentDirectory = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            return currentDirectory;
        }

        internal static char GetSlash()
        {
            string currentDirectory = GetOutputDirectory();
            char slash = currentDirectory.Contains('/') ? '/' : '\\';

            return slash;
        }
    }
}
