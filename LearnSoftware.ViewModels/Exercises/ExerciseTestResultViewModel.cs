﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearnSoftware.ViewModels.Exercises
{
    public class ExerciseTestResultViewModel
    {
        public int Id { get; set; }

        public string Result { get; set; }

        public string Message { get; set; }

        public List<ExerciseTestResultLineViewModel> TestResultLines { get; set; }
    }
}



