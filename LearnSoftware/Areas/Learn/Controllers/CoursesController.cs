﻿using LearnSoftware.Models;
using LearnSoftware.Models.Learn;
using LearnSoftware.Services;
using LearnSoftware.Services.Interfaces;
using LearnSoftware.ViewModels.Courses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LearnSoftware.Areas.Learn.Controllers
{
    [Area("Learn")]
    public class CoursesController : Controller
    {
        private ICourseService courseService;
        private ILectureService lectureService;
        private TextModificationManager textModificationManager;

        public CoursesController(ICourseService courseService, ILectureService lectureService, TextModificationManager textModificationManager)
        {
            this.courseService = courseService;
            this.lectureService = lectureService;
            this.textModificationManager = textModificationManager;
        }

        // GET: /<controller>/
        public IActionResult Index(string courseName, bool inProgress = false, bool purchased = false)
        {
            IEnumerable<Course> coursesQuery;
            if (inProgress)
            {
                coursesQuery = courseService
                .GetCoursesInProgress(this.User.Identity.Name, courseName, this.User.IsInRole("Admin"));
            }
            else if (purchased)
            {
                coursesQuery = courseService
                .GetPurchasedCourses(this.User.Identity.Name, courseName, this.User.IsInRole("Admin"));
            }
            else
            {
                coursesQuery = courseService.GetCourses(courseName, this.User.IsInRole("Admin"));
            }

            var courses = coursesQuery.Select(c => new CourseIndexViewModel()
            {
                Id = c.Id,
                Name = textModificationManager.LimitTextLength(c.Name, 24),
                Description = textModificationManager.LimitTextLength(c.Description, 229),
                LectureCount = c.Lectures.Count,
                CourseType = c.CourseType.ToString(),
                Price = c.CoursePrice,
                IsPurchased = courseService.IsCoursePurchased(c.Id, this.User.Identity.Name)

            }).ToList();

            CoursesIndexContainerViewModel model = new CoursesIndexContainerViewModel()
            {
                Courses = courses,
                InProgress = inProgress,
                Purchased = purchased
            };
            
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("/Courses/ChangeStatus/{courseId}")]
        public IActionResult ChangeStatus(int courseId)
        {
            Course course = courseService.GetCourse((int)courseId);

            if (course == null)
            {
                return this.NotFound();
            }

            courseService.ChangeCourseStatus(course);

            return RedirectToAction("Course","Courses", new { id = courseId });
        }


        [Authorize(Roles = "Admin")]
        [HttpGet("/Courses/Edit/{courseId}")]
        public IActionResult Edit(int courseId)
        {
            Course course = courseService.GetCourse((int)courseId);

            if(course == null)
            {
                return this.NotFound();
            }

            CourseCreateViewModel model = new CourseCreateViewModel()
            {
                Id = courseId,
                CourseType = (int)course.CourseType,
                Description = course.Description,
                Name = course.Name,
                Price = course.CoursePrice
            };

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("/Courses/Edit/{courseId}")]
        public IActionResult Edit(CourseCreateViewModel model)
        {
            if(!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            if (model.CourseType == (int)CourseType.Paid && model.Price == 0)
            {
                ModelState.AddModelError("Price", "Price cannot be 0 if the course is Paid");
                return this.View(model);
            }

            if (model.CourseType == (int)CourseType.Free && model.Price > 0)
            {
                ModelState.AddModelError("Price", "Price cannot be more than 0 if the Course type is Free");
                return this.View(model);
            }

            var course = courseService.GetCourse(model.Id);

            if(course == null)
            {
                return this.NotFound();
            }

            courseService.UpdateCourse(course, (CourseType)model.CourseType, model.Name, model.Description, model.Price);

            return this.RedirectToAction("Course", "Courses", new { id = model.Id });
        }

        [Authorize]
        public IActionResult Course(int id)
        {
            var course = courseService.GetCourse(id);

            if(course == null)
            {
                return this.NotFound();
            }

            var lectures = lectureService.GetLectures(course.Id);

            if(!course.IsActive && !this.User.IsInRole("Admin"))
            {
                return this.Unauthorized();
            }

            var model = new CourseCourseViewModel()
            {
                Id = course.Id,
                Name = course.Name,
                Description = course.Description,
                CourseType = course.CourseType.ToString(),
                IsActive = course.IsActive,
                Price = course.CoursePrice,
                IsPurchased = courseService.IsCoursePurchased(course.Id, this.User.Identity.Name),
                Lectures = lectures.Select(l => new CourseLectureViewModel()
                {
                    Id = l.Id,
                    Name = textModificationManager.LimitTextLength(l.Name, 116),
                    LectureTextExplnation = textModificationManager.LimitTextLength(l.LectureTextExplnation, 286)
                }).ToList()
            };

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            CourseCreateViewModel model = new CourseCreateViewModel() { Id = 0 };
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult Create(CourseCreateViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var courseId = courseService.CreateCourse((CourseType)model.CourseType, model.Name, model.Description, model.Price);

            return RedirectToAction("Course", "Courses", new { id = courseId });
        }
    }
}
