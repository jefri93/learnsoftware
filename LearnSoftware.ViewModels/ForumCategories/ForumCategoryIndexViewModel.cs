﻿using System;
using System.Collections.Generic;

namespace LearnSoftware.ViewModels.ForumCategories
{
    public class ForumCategoryIndexViewModel
    {
        public ForumCategoryIndexViewModel()
        {
            Forums = new List<FocumCategoryIndexForumViewModel>();
        }

        public int Id { get; set; }

        public string ForumCategoryName { get; set; }

        public int MinPage { get; set; }

        public int CurrentPage { get; set; }

        public string Search { get; set; }

        public int MaxPage { get; set; }

        public List<FocumCategoryIndexForumViewModel> Forums { get; set; }
    }
}
