﻿using System;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace LearnSoftware.Services
{
    public class AdminManager<TUser, TRole> : IAdminManager
        where TUser : IdentityUser
        where TRole : IdentityRole
    {
        UserManager<TUser> userManager;
        RoleManager<TRole> roleManager;
        IConfiguration configuration;

        public AdminManager(UserManager<TUser> userManager, RoleManager<TRole> roleManager, IConfiguration configuration)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.configuration = configuration;
        }

        public void CreateRoles()
        {
            bool doesRoleExist = roleManager.RoleExistsAsync("Admin").Result;
            if (!doesRoleExist)
            {
                var role = (TRole)Activator.CreateInstance(typeof(TRole));
                role.Name = "Admin";
                roleManager.CreateAsync(role).Wait();
            }

            doesRoleExist = roleManager.RoleExistsAsync("User").Result;
            if (!doesRoleExist)
            {
                var role = (TRole)Activator.CreateInstance(typeof(TRole));
                role.Name = "User";
                roleManager.CreateAsync(role).Wait();
            }
        }

        public bool CreateDefaultAdmin()
        {
            var user = (TUser)Activator.CreateInstance(typeof(TUser));
            user.UserName = configuration.GetValue<string>("DefaultAdminUserName");
            user.Email = configuration.GetValue<string>("DefaultAdminEmail");

            string userPWD = configuration.GetValue<string>("DefaultAdminPassword");

            IdentityResult chkUser = userManager.CreateAsync(user, userPWD).Result;

            if (chkUser.Succeeded)
            {
                return SetUserRoleAdmin(user.UserName);
            }

            return false;
        }

        public bool SetUserRoleUser(string username)
        {
            return ChangeUserRole(username, "User");
        }

        public bool SetUserRoleAdmin(string username)
        {
            return ChangeUserRole(username, "Admin");
        }

        private bool ChangeUserRole(string username, string role)
        {
            if (!String.IsNullOrEmpty(username))
            {
                var providedUser = userManager.FindByNameAsync(username).Result;

                if (providedUser == null)
                {
                    return false;
                }

                var result = userManager.AddToRoleAsync(providedUser, role).Result;

                return result.Succeeded;
            }

            return false;
        }
    }
}
