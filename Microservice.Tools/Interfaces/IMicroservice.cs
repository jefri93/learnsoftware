﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Microservice.Tools.Interfaces
{
    public interface IMicroservice
    {
        void Run();
    }
}
