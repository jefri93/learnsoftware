ALTER PROCEDURE [dbo].[prcGetComparisonRows]
@TestResultId INT = 0
AS
	SELECT il.TeolText AS ExpectedResult, ol.TolText AS ActualResult FROM (SELECT teol.Id,ROW_NUMBER() OVER (ORDER BY teol.Id) AS iRow, teol.[Text] AS TeolText FROM TestResults AS tr 
	JOIN TestExpectedOutputLines AS teol  ON tr.ExerciseTestId = teol.ExerciseTestId
	WHERE tr.Id = 1) AS il
	JOIN
	(SELECT tol.Id AS TolId, ROW_NUMBER() OVER (ORDER BY tol.Id) AS oRow, tol.[TextLine] AS TolText FROM TestResults AS tr 
	JOIN TestOutputLines AS tol  ON tr.Id = tol.TestResultId
	WHERE tr.Id = 1) AS ol
	ON il.iRow = ol.oRow

GO
