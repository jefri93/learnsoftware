﻿using System.Collections.Generic;
using LearnSoftware.Models.Information;
using LearnSoftware.Models.Learn;
using Microsoft.AspNetCore.Identity;

namespace LearnSoftware.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the LSIdentityUser class
    public class LSIdentityUser : IdentityUser
    {
        public LSIdentityUser()
        {
            ViewedCourses = new List<UserCourse>();
            CreatedForums = new List<Forum>();
            ForumMessages = new List<ForumMessage>();
            Subscriptions = new List<Subscription>();
        }

        public ICollection<Subscription> Subscriptions { get; set; }

        public ICollection<UserCourse> ViewedCourses { get; set; } 

        public ICollection<Forum> CreatedForums { get; set; }

        public ICollection<ForumMessage> ForumMessages { get; set; }

        public ICollection<TestResult> TestResults { get; set; }
    }
}
