﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LearnSoftware.Models.Learn
{
    public class Exercise
    {
        public Exercise()
        {
            ExerciseTests = new List<ExerciseTest>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [NotMapped]
        public bool IsCourseActive { get { return Lecture.IsCourseActive; } }

        [Required]
        public string ExampleInput1 { get; set; }

        [Required]
        public string ExampleOutput1 { get; set; }

        public string ExampleInput2 { get; set; }

        public string ExampleOutput2 { get; set; }

        [Required]
        [ForeignKey("Lecture")]
        public int LectureId { get; set; }

        public Lecture Lecture { get; set; }

        public ICollection<ExerciseTest> ExerciseTests { get; set; }
    }
}
