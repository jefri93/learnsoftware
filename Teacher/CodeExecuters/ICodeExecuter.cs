﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Teacher.CodeExecuters
{
    internal interface ICodeExecuter
    {
        void Execute(int testResultId, string[] classes, long memoryThreshold);

        Task ExecuteAsync(int testResultId, string[] classes, long memoryThreshold);
    }
}
