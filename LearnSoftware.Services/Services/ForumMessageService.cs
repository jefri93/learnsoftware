﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using LearnSoftware.Models.Information;
using LearnSoftware.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LearnSoftware.Services
{
    public class ForumMessageService : IForumMessageService
    {
        private LSIdentityContext context;
        private IPageManager pageService;

        public ForumMessageService(LSIdentityContext context, IPageManager pageService)
        {
            this.context = context;
            this.pageService = pageService;
        }

        public ForumMessage GetForumMessage(int forumMessageId)
        {
            return this.context.ForumMessages.Include(m => m.Forum).Include(m => m.User).FirstOrDefault(m => m.Id == forumMessageId);
        }

        public IEnumerable<ForumMessage> GetForumMessages(int? forumId = null, int? page = null)
        {

            IEnumerable<ForumMessage> messages = context.ForumMessages
                                    .Include(m => m.User)
                                    .Where(m => m.Forum.Id == forumId || forumId == null);
                              
            if (page != null)
            {
                var skip = pageService.GetSkipCount((int)page);
                var take = pageService.ElementsPerPage;

                messages = messages.Skip(skip).Take(take);
            }

            return messages.ToList();
        }

        public int GetCount(int? forumId = null)
        {
            if (forumId != null)
            {
                return context.ForumMessages.Count(f => f.Forum.Id == forumId);
            }

            return context.ForumMessages.Count();
        }

        public void UpdateMessage(ForumMessage message, string messageText)
        {
            message.Message = messageText;
            message.UpdateDate = DateTime.UtcNow;

            this.context.ForumMessages.Update(message);
            this.context.SaveChanges();
        }

        public void AddMessage(Forum forum, LSIdentityUser user, string comment)
        {
            var message = new ForumMessage()
            {
                CreationDate = DateTime.UtcNow,
                UpdateDate = DateTime.UtcNow,
                Forum = forum,
                User = user,
                Message = comment
            };

            this.context.ForumMessages.Add(message);
            this.context.SaveChanges();
        }
    }
}
