﻿using System;
using System.Collections.Generic;
using LearnSoftware.Models.Information;

namespace LearnSoftware.Services.Interfaces
{
    public interface IForumCategoryService
    {
        ForumCategory GetForumCategory(int forumCategoryId);

        bool HasForumCategory(int forumCategoryId);

        bool HasForumCategory(string name);

        IEnumerable<ForumCategory> GetForumCategories();

        string ValidateCategoryName(string name);

        void CreateForumCategory(string name);
    }
}
