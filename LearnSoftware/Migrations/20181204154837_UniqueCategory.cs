﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnSoftware.Migrations
{
    public partial class UniqueCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ForumCategories",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_ForumCategories_Name",
                table: "ForumCategories",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ForumCategories_Name",
                table: "ForumCategories");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ForumCategories",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
