﻿using System;
using System.Collections.Generic;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models.Learn;
using LearnSoftware.Services.Dtos;
using LearnSoftware.ViewModels.Exercises;

namespace LearnSoftware.Services.Interfaces
{
    public interface IExerciseService
    {
        Exercise GetExercise(int id);

        IEnumerable<TestResult> GetTestResults(Exercise exercise, string username, int? page = null);

        int GetTestResultsCount(Exercise exercise, string username);

        void SubmitCode(string code, Exercise exercise, LSIdentityUser user);

        IEnumerable<ExerciseTest> GetExerciseTestsWithIO(Exercise exercise);

        int CreateExercise(
            Lecture lecture, 
            string name,
            string description,
            string exampleInput1,
            string exampleOutput1,
            string exampleInput2,
            string exampleOutput2,
            List<TestLinesViewModel> testLines);

        void UpdateExercise(
            Exercise exercise, 
            string name,
            string description,
            string exampleInput1,
            string exampleOutput1,
            string exampleInput2,
            string exampleOutput2,
            List<TestLinesViewModel> testLines);

        int GetTestProcessingCount();

        UnprocessedTestsDto GetAllUnprocessedTests(string machineId);

        bool UpdateTestResultStatus(UpdateTestResultStatusDto updateTestResultStatusDto);
    }
}
