﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Braintree;
using LearnSoftware.Services;
using LearnSoftware.Services.Interfaces;
using LearnSoftware.ViewModels.Subscriptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LearnSoftware.Areas.Payment.Controllers
{
    [Authorize]
    [Area("Payment")]
    public class SubscriptionController : Controller
    {
        private ISubscriptionService subscriptionService;
        private ICourseService courseService;
        private LSIdentityUserManager lSIdentityUserManager;

        public SubscriptionController(ISubscriptionService subscriptionService, ICourseService courseService, LSIdentityUserManager lSIdentityUserManager)
        {
            this.subscriptionService = subscriptionService;
            this.courseService = courseService;
            this.lSIdentityUserManager = lSIdentityUserManager;
        }

        [HttpGet]
        public IActionResult GenerateToken()
        {
            string token = subscriptionService.GenerateToken();
            return Json(token);
        }

        [HttpGet]
        public IActionResult PurchaseCourse(int id)
        {
            if(subscriptionService.IsUserSubscribed(User.Identity.Name, id))
            {
                return RedirectToAction("Course", "Courses", new { area = "Learn", id = id });
            }

            if (!courseService.HasCourse(id))
            {
                return NotFound();
            }

            var model = subscriptionService.GetCoursePrePaymentData(id, User.Identity.Name);

            return View(model);
        }

        [HttpPost]
        public IActionResult PurchaseCourse(SubscirptionPaymentViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (!courseService.HasCourse(model.course_id))
            {
                return NotFound();
            }

            if (courseService.IsCourseFree(model.course_id))
            {
                return BadRequest();
            }

            var response = subscriptionService.GetCoursePrePaymentData(model.course_id, User.Identity.Name);
            var user = lSIdentityUserManager.FindByNameAsync(User.Identity.Name).Result;
            if (user == null || !lSIdentityUserManager.IsEmailConfirmedAsync(user).Result)
            {
                return View(response);
            }

            var paymentResponse = subscriptionService.MakePayment(model, User.Identity.Name);
            response.TransactionId = paymentResponse.TransactionId;
            response.Message = paymentResponse.ErrorMessage;

            return View(response);
        }
    }
}