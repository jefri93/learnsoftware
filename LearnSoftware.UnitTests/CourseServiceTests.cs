using System;
using System.Collections.Generic;
using System.Linq;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using LearnSoftware.Models.Learn;
using LearnSoftware.Services;
using LearnSoftware.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using Xunit;

namespace LearnSoftware.UnitTests
{
    public class CourseServiceTests
    {
        private ICourseService courseService;

        public CourseServiceTests()
        {
            courseService = new CourseService(TestData.GetDataFilledContext());
        }

        [Fact]
        public void ChangeStatusTest()
        {
            Course course = courseService.GetCourse(1);

            courseService.ChangeCourseStatus(course);

            Assert.True(course.IsActive);
        }

        [Fact]
        public void GetCoursesAdminTest()
        {
            var courses = courseService.GetCourses(null, true);

            Assert.True(2 == courses.Count());
        }

        [Fact]
        public void GetCoursesUserTest()
        {
            var courses = courseService.GetCourses(null, false);

            Assert.True(0 == courses.Count());
        }

        [Fact]
        public void GetCoursesSearchTest()
        {
            var courses = courseService.GetCourses("2", true);
            int result = courses.Count();
            Assert.True(1 == result);
        }

        [Fact]
        public void GetCoursesInProgressTest()
        {
            var courses = courseService.GetCoursesInProgress("User",null, true);
            int result = courses.Count();
            Assert.True(1 == result);
        }

        [Fact]
        public void CreateCourseTest()
        {
            courseService.CreateCourse(CourseType.Free, "Course 3", "kkkkk", 25);
            var courses = courseService.GetCourses("3", true);
            int result = courses.Count();
            Assert.True(1 == result);
        }

        [Fact]
        public void UpdateCourseTest()
        {
            var course = courseService.GetCourse(6);
            courseService.UpdateCourse(course,CourseType.Free, "Course 3", "ahahahhaah", 25);

            Assert.True(course.Description == "ahahahhaah");
        }
    }
}
