﻿using LearnSoftware.Areas.Identity.Data;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace LearnSoftware.Services
{
    public class LSIdentityUserManager : UserManager<LSIdentityUser>
    {
        LSIdentityContext context;
        IEmailSender emailSender;

        public LSIdentityUserManager(LSIdentityContext context, IUserStore<LSIdentityUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<LSIdentityUser> passwordHasher, IEnumerable<IUserValidator<LSIdentityUser>> userValidators, IEnumerable<IPasswordValidator<LSIdentityUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<LSIdentityUser>> logger, IEmailSender emailSender) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            this.context = context;
            this.emailSender = emailSender;
        }

        public LSIdentityUser GetUserWithCustomProperties(string username)
        {
            return context.Users
                .Include(u => u.TestResults)
                .Include(u => u.ForumMessages)
                .Include(u => u.CreatedForums)
                .FirstOrDefault(u => u.UserName == username);
        }

        public async Task<bool> SendVerificationEmail(LSIdentityUser user, string inputEmail, string callbackUrl)
        {
            var email = await this.GetEmailAsync(user);
            if (inputEmail != email)
            {
                var emailUser = this.FindByEmailAsync(inputEmail).Result;

                if (emailUser != null)
                {
                    return false;
                }

                var setEmailResult = await this.SetEmailAsync(user, inputEmail);
                if (setEmailResult.Succeeded)
                {
                    email = inputEmail;
                }
            }

            await emailSender.SendEmailAsync(
                email,
                "Confirm your email",
                $"<h3>Please confirm your E-mail by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.</h3><h3>If that does not work please go to {HtmlEncoder.Default.Encode(callbackUrl)} .");

            return true;
        }
    }
}
