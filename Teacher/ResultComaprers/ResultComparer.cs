﻿using Microservice.Tools.Sevices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Teacher.Dtos;

namespace Teacher.ResultComaprers
{
    public class ResultComparer : IResultComparer
    {
        public CompareResult Compare(int testResultId)
        {
            var linesToRead = FileService.Read<TestIOContainer>(testResultId);
            var expectedResult = GetColumnValues(linesToRead.ExpectedOutputLines);
            var actualResult = GetColumnValues(linesToRead.OutputLines);

            return Compare(expectedResult, actualResult);
        }

        private List<string> GetColumnValues(string text)
        {
            var result = text.Split(Environment.NewLine).Select(l => l.TrimEnd())
                              .ToList();

            return result;
        }

        private string GetCurrentCompareIndex(int index, List<string> lines)
        {
            return lines.Count - 1 >= index ? lines[index] : String.Empty;
        }

        public CompareResult Compare(List<string> expectedResultLines, List<string> actualResultLines)
        {
            int loopLength = expectedResultLines.Count < actualResultLines.Count
                ? actualResultLines.Count : expectedResultLines.Count;

            List<ResultLineDto> resultLines = new List<ResultLineDto>();

            bool isScuccess = true;
            for(int i = 0; i < loopLength;i++)
            {
                var currentExpectedResult = GetCurrentCompareIndex(i, expectedResultLines);
                var currentActualResult = GetCurrentCompareIndex(i, actualResultLines);

                bool isEqual = currentExpectedResult == currentActualResult;
                string isMatchMessage = isEqual ? "Yes" : "No";

                if (!(loopLength - 1 == i && currentActualResult == String.Empty && currentExpectedResult == String.Empty))
                {
                    resultLines.Add(new ResultLineDto() { Number = i + 1, Match = isMatchMessage, ExpectedResult = currentExpectedResult, ActualResult = currentActualResult });
                }

                if(!isEqual)
                {
                    isScuccess = false;
                }
            }

            return new CompareResult(isScuccess, isScuccess ? null : JsonConvert.SerializeObject(resultLines));
        }


    }
}
