﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;

namespace Microservice.Tools.Sevices
{
    public static class FileService
    {
        private static string CurrentDirectory 
        { 
            get 
            {
                string assemblyLocation = Assembly.GetExecutingAssembly().GetName().CodeBase.Replace("file:///", String.Empty);
                string currentDictionary = Path.GetDirectoryName(assemblyLocation) + "\\TestResultsData\\";
                Directory.CreateDirectory(currentDictionary);
                return currentDictionary;
            } 
        }

        public static void Write<T>(int id, T t)
        {
            if(t == null)
            {
                throw new ArgumentNullException();
            }

            string json = JsonConvert.SerializeObject(t);
            File.WriteAllText(CurrentDirectory + id, json);
        }

        public static T Read<T>(int id)
        {
            string json = File.ReadAllText(CurrentDirectory + id);
            var t = JsonConvert.DeserializeObject<T>(json);
            return t;
        }

        public static void Delete(int id)
        {
            File.Delete(CurrentDirectory + id);
        }
    }
}
