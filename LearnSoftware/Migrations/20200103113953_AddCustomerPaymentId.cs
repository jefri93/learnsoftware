﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnSoftware.Migrations
{
    public partial class AddCustomerPaymentId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustomerPaymentId",
                table: "Subscriptions",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_CourseId",
                table: "Subscriptions",
                column: "CourseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscriptions_Courses_CourseId",
                table: "Subscriptions",
                column: "CourseId",
                principalTable: "Courses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscriptions_Courses_CourseId",
                table: "Subscriptions");

            migrationBuilder.DropIndex(
                name: "IX_Subscriptions_CourseId",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "CustomerPaymentId",
                table: "Subscriptions");
        }
    }
}
