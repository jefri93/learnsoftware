﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Teacher.Dtos
{
    public class ResultLineDto
    {
        public int Number { get; set; }

        public string Match { get; set; }

        public string ExpectedResult { get; set; }

        public string ActualResult { get; set; }
    }
}
