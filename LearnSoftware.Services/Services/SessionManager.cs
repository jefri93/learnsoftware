﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using LearnSoftware.Services.Interfaces;
using Microsoft.AspNetCore.Http;

namespace LearnSoftware.Services
{
    public class SessionManager : ISessionManager
    {
        public byte[] ObjectToByteArray<T>(T obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public T ByteArrayToObject<T>(byte[] arrBytes)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return (T)obj;
            }
        }

        public T GetSessionObject<T>(string key, ISession session)
        {
            byte[] bytesTests = session.Get(key);

            T item = bytesTests == null || bytesTests.Length == 0 ? default(T) : this.ByteArrayToObject<T>(bytesTests);

            return item;
        }

        public void SetSessionObject<T>(string key, T obj, ISession session)
        {
            byte []bytesTests = this.ObjectToByteArray(obj);
            session.Set(key, bytesTests);
        }
    }
}
