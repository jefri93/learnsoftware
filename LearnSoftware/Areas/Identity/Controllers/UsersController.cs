﻿using System;
using System.Linq;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Controllers;
using LearnSoftware.Models;
using LearnSoftware.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LearnSoftware.Services.Interfaces;
using LearnSoftware.ViewModels.Users;
using LearnSoftware.ViewModels;

namespace LearnSoftware.Areas.Identity.Controllers
{
    [Area("Identity")]
    public class UsersController : Controller
    {
        private IAdminManager adminManager;
        private LSIdentityUserManager userService;
        private ICourseService courseService;
        private IPageManager pageManager;

        public UsersController(ICourseService courseService, IPageManager pageManager, LSIdentityUserManager userService, IAdminManager adminManager)
        {
            this.adminManager = adminManager;
            this.userService = userService;
            this.pageManager = pageManager;
            this.courseService = courseService;
        }

        [Authorize]
        [HttpGet("/Users/UserProfile/{username}")]
        public IActionResult UserProfile(string username)
        {
            if (String.IsNullOrEmpty(username))
            {
                return this.BadRequest();
            }

            var user = userService.GetUserWithCustomProperties(username);
            var courses = courseService.GetCourses(null, this.User.IsInRole("Admin"))
                                                   .Where(c => c.UsersCourses.Any(uc => uc.UserId == user.Id))
                                                   .OrderByDescending(c => c.Id)
                                                   .Take(10)
                                                   .ToDictionary(k => k.Id, v => v.Name);

            if (user == null)
            {
                return this.NotFound();
            }

            UserUserProfileViewModel model = new UserUserProfileViewModel()
            {
                UserName = user.UserName,
                ResultSuccessCount = user.TestResults.Count(tr => tr.Success == true),
                ResultFailedCount = user.TestResults.Count(tr => tr.Success == false),
                ResultTotalCount = user.TestResults.Count(),
                CreatedForums = user.CreatedForums.OrderByDescending(cf => cf.CreationDate).Take(pageManager.ElementsPerPage).ToDictionary(k => k.Id, v => v.Name),
                ForumMessages = user.ForumMessages.OrderByDescending(fm => fm.CreationDate).Take(pageManager.ElementsPerPage).ToDictionary(k => k.Id, v => v.Message),
                ViewedCourses = courses,
                IsAdmin = userService.IsInRoleAsync(user, "Admin").Result
            };

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("/Users/MakeAdmin/{username}")]
        public IActionResult MakeAdmin(string username)
        {
            if(String.IsNullOrEmpty(username))
            {
                return this.BadRequest();
            }

            UserMakeAdminViewModel model = new UserMakeAdminViewModel(){ Username = username };

            return this.View(model);
        }

        [HttpPost("/Users/MakeAdmin/{username}")]
        [Authorize(Roles = "Admin")]
        public IActionResult MakeAdmin(UserMakeAdminViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var user = userService.FindByNameAsync(model.Username).Result;

            if(user == null)
            {
                return this.NotFound();
            }

            var admin = userService.FindByNameAsync(model.AdminUsername).Result;

            if(admin == null)
            {
                return this.Unauthorized();
            }

            bool isAuthenticated = this.userService.CheckPasswordAsync(admin, model.AdminPassword).Result
                && model.AdminUsername == this.User.Identity.Name;

            if(!isAuthenticated)
            {
                return this.Unauthorized();
            }

            if(admin.UserName != this.User.Identity.Name)
            {
                return this.Unauthorized();
            }

            adminManager.SetUserRoleAdmin(model.Username);

            return this.RedirectToAction("UserProfile", "Users", new { username = model.Username });
        }

        [Authorize]
        [HttpPost]
        public IActionResult SendVerificationEmail(SendVerificationEmailViewModel model)
        {
            var user = userService.GetUserAsync(User).Result;
            if (user == null)
            {
                return BadRequest();
            }

            var code = userService.GenerateEmailConfirmationTokenAsync(user).Result;
            var userId = user.Id;

            var callbackUrl = Url.Page(
                     "/Identity/Account/ConfirmEmail",
                     pageHandler: null,
                     values: new { userId, code },
                     protocol: Request.Scheme);

            var success = userService.SendVerificationEmail(user, model.Email, callbackUrl).Result;
            return this.Redirect(model.RedirectUrl);
        }
    }
}