﻿using System;
using System.ComponentModel.DataAnnotations;
using LearnSoftware.Areas.Identity.Data;

namespace LearnSoftware.Models.Information
{
    public class Message
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public LSIdentityUser Sender { get; set; }

        [Required]
        public MessageConnection MessageConnection { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }
    }
}
