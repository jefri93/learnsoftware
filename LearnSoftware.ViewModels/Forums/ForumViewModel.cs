﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LearnSoftware.ViewModels.Forums
{
    public class ForumViewModel
    {
        public ForumViewModel()
        {
            Messages = new List<ForumMessageViewModel>();
        }

        public int Id { get; set; }

        public bool IsActive { get; set; }

        public int MinPage { get; set; }

        public int MaxPage { get; set; }

        public int CurrentPage { get; set; }

        public string Name { get; set; }

        public List<ForumMessageViewModel> Messages { get; set; }

        public string CreationDate { get; set; }

        public string UpdateDate { get; set; }

        public string CreatorUsername { get; set; }

        [Required]
        public string NewComment { get; set; }
    }
}
