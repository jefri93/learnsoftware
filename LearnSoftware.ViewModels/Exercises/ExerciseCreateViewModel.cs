﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LearnSoftware.ViewModels.Exercises
{
    public class ExerciseCreateViewModel
    {
        public ExerciseCreateViewModel()
        {
            TestLines = new List<TestLinesViewModel>();
        }
        [Required]
        public int Id { get; set; }

        [Required]
        public int LectureId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public string ExampleInput1 { get; set; }

        [Required]
        public string ExampleOutput1 { get; set; }

        public string ExampleInput2 { get; set; }

        public string ExampleOutput2 { get; set; }

        public TestLinesViewModel CurrentTestLines { get; set; }

        public List<TestLinesViewModel> TestLines { get; set; }
    }
}
