﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LearnSoftware.ViewModels.Subscriptions
{
    public class SubscriptionPrePaymentViewModel
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public int LectureCount { get; set; }

        public int VideoTime { get; set; }

        public int ExerciseCount { get; set; }

        public decimal Price { get; set; }

        public string Message { get; set; }

        public string TransactionId { get; set; }

        public bool IsPurchased { get; set; }

        public bool IsEmailVerified { get; set; }

        public string Email { get; set; }
    }
}
