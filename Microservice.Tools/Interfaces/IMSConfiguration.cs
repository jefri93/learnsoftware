﻿using System;
namespace Microservice.Tools.Interfaces
{
    public interface IMSConfiguration
    {
        int TimeIntervalMs { get; set; }

        int Timeout { get; set; } 

        int CodeBlockByteLimit { get; set; }

        int TestResultProcessingThreadCount { get; set; }

        string[] ForbiddenKeywords { get; set; }

        int MaxProcessedCount { get; set; }

        long MemoryThreshold { get; set; }
    }
}
