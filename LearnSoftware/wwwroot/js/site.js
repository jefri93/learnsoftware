﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function createBraintreeView() {
    var courseInput = document.querySelector('#courseId');
    if (courseInput !== undefined) {
        var message = document.querySelector('#braintree-message');
        var braintreeContainer = document.querySelector("#dropin-container");
        $.ajax({
            url: "/Payment/Subscription/GenerateToken",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                var clientToken = res;

                braintree.setup(clientToken, "dropin", {
                    container: "payment-form"
                });
            },
            error: function (data) {
                message.innerHTML = 'Something went wrong, please refresh page.'
            }
        });
    }
}

createBraintreeView();