﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Teacher.Exceptions
{
    public class TeacherException : Exception
    {
        public TeacherException(string message)
            :base(message)
        {

        }
    }
}
