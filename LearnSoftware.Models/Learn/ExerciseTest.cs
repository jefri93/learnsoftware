﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LearnSoftware.Models.Learn
{
    public class ExerciseTest
    {
        public ExerciseTest()
        {
            TestInputs = new List<TestInputLine>();
            TestResults = new List<TestResult>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public Exercise Exercise { get; set; }

        [Required]
        public string TestExpectedOutputLines { get; set; }

        public ICollection<TestInputLine> TestInputs { get; set; }

        public ICollection<TestResult> TestResults { get; set; }
    }
}
