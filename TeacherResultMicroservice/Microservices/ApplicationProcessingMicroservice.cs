﻿using LoggerLite;
using Microservice.Tools;
using Microservice.Tools.Interfaces;
using Microservice.Tools.Services;
using Microservice.Tools.Sevices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Teacher.CodeExecuters;
using Teacher.Dtos;
using Teacher.Exceptions;
using Teacher.ResultComaprers;

namespace Microservices.Microservices
{
    public class ApplicationProcessingMicroservice : BaseMicroservice
    {
        private IResultComparer comparer;
        private BaseCodeExecuter executer;
        private LearnSoftwareExerciseTestsService learnSoftwareExerciseTestsApi;
        private int processedCount;

        public ApplicationProcessingMicroservice(BaseCodeExecuter executer, IResultComparer comparer, ILogger logger, IMSConfiguration configuration, LearnSoftwareExerciseTestsService learnSoftwareExerciseTestsApi) 
            : base(logger, configuration)
        {
            this.executer = executer;
            this.comparer = comparer;
            this.learnSoftwareExerciseTestsApi = learnSoftwareExerciseTestsApi;
            this.processedCount = 0;
        }

        public override void Run()
        {
            Logger.LogInfo("ApplicationProcessingMicroservice has started.");
            Console.WriteLine("ApplicationProcessingMicroservice has started.");
            while (true)
            {
                //can make async when AppDomain creation added to .Net Core
                ProcessApplications();
                int timeIntervalInMs = learnSoftwareExerciseTestsApi.GetWaitTimeInMS();
                Thread.Sleep(timeIntervalInMs);
            }
        }

        private void ProcessApplications()
        {
            try
            {
                ProcessClasses();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                Console.WriteLine(e.Message);
            }
        }

        private void WriteFiles(List<TestIOContainer> list)
        {
            foreach(var item in list)
            {
                FileService.Write(item.TestResultId, item);
            }
        }

        private void ProcessClasses()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var dto = learnSoftwareExerciseTestsApi.GetUnprocessedTests();

            if(dto.GroupedClasses.Count == 0)
            {
                Console.WriteLine("prcGetUnprocessedTestResults returned 0 results");
                return;
            }

            WriteFiles(dto.TestIOContainers);

            List<Task> tasks = new List<Task>();
            int threadCount = dto.GroupedClasses.Count < Configuration.TestResultProcessingThreadCount 
                                            ? dto.GroupedClasses.Count : Configuration.TestResultProcessingThreadCount;

            int amountOfTasks = (dto.GroupedClasses.Count / threadCount);

            //Wait till AppDomain is included to be able to individually monitor memory
            //For memory monitoring
            //https://stackoverflow.com/questions/2342023/how-to-measure-the-total-memory-consumption-of-the-current-process-programmatica

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            long memoryBefore = Process.GetCurrentProcess().VirtualMemorySize64;
            long memoryThreshold = memoryBefore + Configuration.MemoryThreshold;

            for (int i = 1; i <= threadCount;i++)
            {
                Task task = new Task((object obj) =>
                {
                    int taskIndex = ((int)obj);
                    int endpoint = taskIndex * (dto.GroupedClasses.Count / threadCount);
                    int startPoint = endpoint - amountOfTasks;

                    var threadPartition = dto.GroupedClasses.Skip(startPoint).Take(amountOfTasks);

                    foreach (var group in threadPartition)
                    {
                        int testResultId = group.Key;
                        string[] classes = group.ValueList.ToArray();

                        Console.WriteLine($"Retrived TestResultId: {testResultId}");
                        Logger.LogInfo($"Retrived TestResultId: {testResultId}");

                        ProcessClassGroup(testResultId, classes, memoryThreshold);
                        FileService.Delete(testResultId);
                    }
                },(object)i, CancellationToken.None, TaskCreationOptions.LongRunning);  

                tasks.Add(task);
                task.Start();
            }

            Task.WhenAll(tasks).Wait();
            stopwatch.Stop();
            processedCount += dto.GroupedClasses.Count;

            Logger.LogInfo($"Processed batch; TestResults: {dto.GroupedClasses.Count}; Classes: {dto.GroupedClasses.SelectMany(gc => gc.ValueList).Count()}; Total time: {stopwatch.Elapsed};");

            if (processedCount >= Configuration.MaxProcessedCount && processedCount != 0)
            {
                Logger.LogInfo($"App restarting...");
                Process.Start("executer.bat");
                Environment.Exit(0);
            }
        }

        private void ProcessClassGroup(int testResultId, string[] classes, long memoryThreshold)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            try
            {
                executer.Execute(testResultId, classes, memoryThreshold);

                var result = comparer.Compare(testResultId);
                learnSoftwareExerciseTestsApi.UpdateTestResultStatus(testResultId, null, result.Success, result.Json);
            }
            catch(TeacherException e)
            {
                string message = e.InnerException == null
                                  ? e.Message : e.InnerException.InnerException == null
                                  ? e.InnerException.Message : e.InnerException.InnerException.Message;

                Console.WriteLine($"TestResultId {testResultId} error: {message}");
                Logger.LogError($"TestResultId {testResultId} error: {message}");

                learnSoftwareExerciseTestsApi.UpdateTestResultStatus(testResultId, e.Message, false);
            }
            catch(Exception e)
            {
                string message = e.InnerException == null 
                                  ? e.Message : e.InnerException.InnerException ==null 
                                  ? e.InnerException.Message : e.InnerException.InnerException.Message;

                Console.WriteLine($"TestResultId {testResultId} error: Exception has been thrown.");
                Logger.LogError($"TestResultId {testResultId} error: {message}");

                learnSoftwareExerciseTestsApi.UpdateTestResultStatus(testResultId, message, false);
            }

            watch.Stop();

            Console.WriteLine($"TestResultId {testResultId} Processed for {watch.Elapsed}");
            Logger.LogInfo($"TestResultId {testResultId} Processed for {watch.Elapsed}");
        }
    }
}
