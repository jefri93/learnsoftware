﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LearnSoftware.Models.Learn
{
    public class Lecture
    {
        public Lecture()
        {
            Exercices = new List<Exercise>();
        }

        [Key]
        public int Id { get; set; }

        public string LectureVideoUrl { get; set; }

        public long VideoLength { get; set; }

        [NotMapped]
        public bool IsCourseActive { get { return Course.IsActive; } }

        [Required]
        public string Name { get; set; }

        public string LectureTextExplnation { get; set; }

        public ICollection<Exercise> Exercices { get; set; }

        [Required]
        [ForeignKey("Course")]
        public int CourseId { get; set; }

        public Course Course { get; set; }
    }
}
