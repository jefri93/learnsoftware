﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LearnSoftware.Services.Dtos
{
    public class UpdateTestResultStatusDto
    {
        [Required]
        public string MachineId { get; set; }

        [Required]
        public int TestResultId { get; set; }

        public string Message { get; set; }

        public string Json { get; set; }

        [Required]
        public bool Success { get; set; }
    }
}
