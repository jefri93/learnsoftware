﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LearnSoftware.Models.Learn
{
    public class TestResultLine
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int LineNumber { get; set; }

        [Required]
        public string Match { get; set; }

        [Required]
        public string ExpectedResult { get; set; }

        [Required]
        public string ActualResult { get; set; }

        [Required]
        [ForeignKey("TestResult")]
        public int TestResultId { get; set; }

        public TestResult TestResult { get; set; }
    }
}
