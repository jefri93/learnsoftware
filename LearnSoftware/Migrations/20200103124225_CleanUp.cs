﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnSoftware.Migrations
{
    public partial class CleanUp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerPaymentId",
                table: "Subscriptions");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "Subscriptions",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "CoursePrice",
                table: "Courses",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "CoursePrice",
                table: "Courses");

            migrationBuilder.AddColumn<string>(
                name: "CustomerPaymentId",
                table: "Subscriptions",
                nullable: false,
                defaultValue: "");
        }
    }
}
