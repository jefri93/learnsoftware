﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnSoftware.Services.Interfaces;
using Microsoft.Extensions.Configuration;

namespace LearnSoftware.Services
{
    public class PageManager : IPageManager
    {
        private IConfiguration configuration;

        public PageManager(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public int ElementsPerPage => configuration.GetValue<int>("ElementsPerPage");

        public int GetMaxPage(int itemCount)
        {
            var roughResult = (int)Math.Ceiling((double)itemCount / ElementsPerPage);
            var finalResult = roughResult == 1 ? 1 : roughResult + 1;

            return finalResult;
        }

        public int GetMinPage(int maxPage)
        {
            return maxPage > 0 ? 1 : 0;
        }

        public int GetSkipCount(int page)
        {
            return (page - 1) * ElementsPerPage;
        }
    }
}
