﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using Microsoft.EntityFrameworkCore;

namespace LearnSoftware.Data
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private LSIdentityContext context;

        private DbSet<TEntity> dbSet;

        public Repository(LSIdentityContext context)
        {
            this.context = context;

            this.dbSet = this.context.Set<TEntity>();
        }

        public IQueryable<TEntity> All() => this.dbSet;

        public void Add(TEntity entity) => this.dbSet.Add(entity);

        public void Update(TEntity entity)
        {
            dbSet.Update(entity);
        }

        public void Delete(TEntity entity)
        {
            dbSet.Remove(entity);
        }

        public Task<int> SaveChangesAsync()
        {
            return context.SaveChangesAsync();
        }
    }
}
