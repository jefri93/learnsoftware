﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LearnSoftware.Areas.Identity.Data;

namespace LearnSoftware.Models.Learn
{
    public class Subscription
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [ForeignKey("Course")]
        public int CourseId { get; set; }

        public Course Course { get; set; }

        [Required]
        [ForeignKey("User")]
        public string UserId { get; set; }

        public LSIdentityUser User { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }
    }
}
