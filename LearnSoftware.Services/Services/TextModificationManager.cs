﻿using System;
namespace LearnSoftware.Services
{
    public class TextModificationManager
    {
        public string LimitTextLength(string text, int maxLength)
        {
            if(maxLength < 1)
            {
                throw new ArgumentException("maxLength cannot be less than 1");
            }

            bool textIsMoreThanMaxLength = text.Length > maxLength;
            bool shouldIncludeDots = maxLength > 3;

            if (textIsMoreThanMaxLength)
            {
                text = text.Substring(0, shouldIncludeDots ? maxLength - 3 : maxLength) + (shouldIncludeDots ? "..." : String.Empty);
            }

            return text;
        }
    }
}
