﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LearnSoftware.ViewModels
{
    public class MakeAdminViewModel
    {
        [Required]
        public string AdminUsername { get; set; }

        [Required]
        public string AdminPassword { get; set; }
    }
}
