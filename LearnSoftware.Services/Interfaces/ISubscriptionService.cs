﻿using LearnSoftware.ViewModels.Subscriptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace LearnSoftware.Services.Interfaces
{
    public interface ISubscriptionService
    {
        string GenerateToken(string username = null);

        SubscriptionPaymentProcessedViewModel MakePayment(SubscirptionPaymentViewModel model, string username);

        SubscriptionPrePaymentViewModel GetCoursePrePaymentData(int courseId, string username);

        bool IsUserSubscribed(string username, int courseId);
    }
}
