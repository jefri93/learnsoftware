﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LearnSoftware.ViewModels.Courses
{
    public class CourseCreateViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Range(0,1)]
        public int CourseType { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public string Description { get; set; }

    }
}
