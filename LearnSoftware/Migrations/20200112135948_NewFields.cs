﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnSoftware.Migrations
{
    public partial class NewFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MachineId",
                table: "TestResultsTestClasses",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SentForProcessingAt",
                table: "TestResultsTestClasses",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MachineId",
                table: "TestResultsTestClasses");

            migrationBuilder.DropColumn(
                name: "SentForProcessingAt",
                table: "TestResultsTestClasses");
        }
    }
}
