﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LearnSoftware.ViewModels.Subscriptions
{
    public class SubscriptionPaymentProcessedViewModel
    {
        public string TransactionId { get; set; }

        public string ErrorMessage { get; set; }
    }
}
