﻿using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Services;
using LearnSoftware.Services.Interfaces;
using LearnSoftware.Services.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LearnSoftware
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession();
            services.AddAuthorization();
            services.AddAuthentication();
            services.AddSingleton<ISessionManager, SessionManager>();
            services.AddScoped<ISubscriptionService, SubscriptionService>();
            services.AddScoped<IAdminManager, AdminManager<LSIdentityUser, IdentityRole>>();
            services.AddSingleton<ITempDataProvider, CookieTempDataProvider>();
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddScoped<IPageManager, PageManager>();
            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<IExerciseService, ExerciseService>();
            services.AddScoped<IForumCategoryService, ForumCategoryService>();
            services.AddScoped<IForumMessageService, ForumMessageService>();
            services.AddScoped<IForumService, ForumService>();
            services.AddScoped<IEmailSender, EmailService>();
            services.AddScoped<TextModificationManager>();
            services.AddScoped<ILectureService, LectureService>();
            services.AddScoped<LSIdentityUserManager>();
            services.AddScoped<ILoadBalancingService, LoadBalancingService>();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                     name: "areas",
                     template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                   );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
