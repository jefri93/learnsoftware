CREATE PROCEDURE prcUpdateTestResultStatus   
    @TestResultId INT = 0,
	@Success BIT = NULL,
	@Message NVARCHAR(MAX) = NULL	
AS   
BEGIN TRANSACTION;
	UPDATE TestResults WITH(XLOCK, ROWLOCK) SET Success = @Success, [Message] = @Message  WHERE Id = @TestResultId;
COMMIT TRANSACTION;
GO


