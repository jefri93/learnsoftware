﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Teacher.Dtos;

namespace Microservice.Tools.Services
{
    public class LearnSoftwareExerciseTestsService
    {
        private string url, username, password, machineId;

        public LearnSoftwareExerciseTestsService(string url, string username, string password)
        {
            this.url = url;
            this.username = username;
            this.password = password;
            this.machineId = new Guid().ToString();
        }

        private string HttpGet(string URI)
        {
            string responseText = null;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URI);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes($"{username}:{password}")));
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception($"Http GET returned {response.StatusCode}.");
                }

                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    responseText = reader.ReadToEnd();
                }
            }

            return responseText;
        }

        private async Task<string> HttpPost(Uri url, Dictionary<string, string> values)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes($"{username}:{password}")));

                using (var content = new FormUrlEncodedContent(values))
                {
                    var response = await client.PostAsync(url, content).ConfigureAwait(true);
                    var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(true);

                    return responseString;
                }
            }
        }

        public UnprocessedTestsDto GetUnprocessedTests()
        {
            string requestUrl = $"{url}Home/GetAllUnprocessedTests/{machineId}";
            var json = HttpGet(requestUrl);
            var dto = JsonConvert.DeserializeObject<UnprocessedTestsDto>(json);

            return dto;
        }

        public int GetWaitTimeInMS()
        {
            string requestUrl = $"{url}Home/GetSleepMiliseconds";
            int result;
            try
            {
                if (!int.TryParse(HttpGet(requestUrl), out result))
                {
                    result = 1000;
                }
            }
            catch
            {
                result = 1000;
            }

            return result;
        }

        public void UpdateTestResultStatus(int testResultId, string message, bool success, string json = null)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            values.Add("Username", username);
            values.Add("Password", password);
            values.Add("MachineId", machineId);
            values.Add("TestResultId", testResultId.ToString());
            values.Add("Message", message);
            values.Add("Success", success.ToString().ToLower());
            values.Add("Json", json);

            var result = HttpPost(new Uri($"{url}Home/UpdateTestResultStatus"), values).Result;
        }
    }
}
