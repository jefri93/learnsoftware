﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LearnSoftware.Models.Learn
{
    public class TestClass
    {
        public TestClass()
        {
            TestResultsTestClasses = new List<TestResultTestClass>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Class { get; set; }

        [Required]
        public ICollection<TestResultTestClass> TestResultsTestClasses { get; set; }
    }
}
