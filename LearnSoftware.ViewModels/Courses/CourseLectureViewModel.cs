﻿using System;
using System.Collections.Generic;

namespace LearnSoftware.ViewModels.Courses
{
    public class CourseLectureViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LectureTextExplnation { get; set; }


    }
}
