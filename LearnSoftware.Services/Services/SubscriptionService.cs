﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Braintree;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models.Learn;
using LearnSoftware.Services.Interfaces;
using LearnSoftware.ViewModels.Subscriptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace LearnSoftware.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        private LSIdentityContext context;
        private IConfiguration configuration;
        private ICourseService courseService;

        public SubscriptionService(LSIdentityContext context, ICourseService courseService, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
            this.courseService = courseService;
        }

        private BraintreeGateway GetGateway()
        {
            return new BraintreeGateway
            {
                Environment = configuration.GetValue<string>("Environment") == "DEV" ? Braintree.Environment.SANDBOX : Braintree.Environment.PRODUCTION,
                MerchantId = configuration.GetValue<string>("MerchantID"),
                PublicKey = configuration.GetValue<string>("PublicKey"),
                PrivateKey = configuration.GetValue<string>("PrivateKey")
            };
        }

        public string GenerateToken(string username = null)
        {
            var gateway = GetGateway();
            string clientToken = gateway.ClientToken.Generate();
            return clientToken;
        }

        public SubscriptionPaymentProcessedViewModel MakePayment(SubscirptionPaymentViewModel model, string username)
        {
            var response = new SubscriptionPaymentProcessedViewModel()
            {
                ErrorMessage = String.Empty,
            };

            var gateway = GetGateway();

            var course = context.Courses.FirstOrDefault(c => c.Id == model.course_id);

            var request = new TransactionRequest
            {
                Amount = course.CoursePrice,
                PaymentMethodNonce = model.payment_method_nonce,
                Options = new TransactionOptionsRequest
                {
                    SubmitForSettlement = true
                }
            };

            var user = context.Users.FirstOrDefault(u => u.UserName == username);

            if(user == null)
            {
                response.ErrorMessage = "User not logged in.";
                return response;
            }

            Result<Transaction> result = gateway.Transaction.Sale(request);
            if (result.IsSuccess())
            {
                context.Subscriptions.Add(new Models.Learn.Subscription()
                {
                    CourseId = model.course_id,
                    UserId = user.Id,
                    CreationDate = DateTime.UtcNow
                });

                SafeContextSave();
                response.TransactionId = result.Target.Id;
            }
            else
            {
                response.ErrorMessage = "Payment did not go trough, please try again. If that does not work please contact your bank.";
            }

            return response;
        }

        private void SafeContextSave(int retryCount = 0, int waitUntilRetry = 1000)
        {
            try
            {
                context.SaveChanges();
            }
            catch
            {
                if(retryCount < 3)
                {
                    retryCount++;
                    Thread.Sleep(waitUntilRetry);
                    SafeContextSave(retryCount, waitUntilRetry * 2);
                }
            }
        }

        public SubscriptionPrePaymentViewModel GetCoursePrePaymentData(int courseId, string username)
        {
            var course = context.Courses.Include(c => c.Lectures).FirstOrDefault(c => c.Id == courseId);

            var clientToken = GenerateToken();

            var exercisesCount = context.Lectures.Include(l => l.Exercices).Where(l => l.CourseId == courseId).Sum(l => l.Exercices.Count);
            var user = context.Users.FirstOrDefault(u => u.UserName == username);
            SubscriptionPrePaymentViewModel model = new SubscriptionPrePaymentViewModel()
            {
                CourseId = courseId,
                CourseName = course.Name,
                LectureCount = course.Lectures.Count,
                ExerciseCount = exercisesCount,
                VideoTime = course.Lectures.Sum(l => (int)Math.Round(new TimeSpan(l.VideoLength).TotalHours)),
                Message = String.Empty,
                Price = course.CoursePrice,
                IsPurchased = courseService.IsCoursePurchased(courseId, username),
                IsEmailVerified = user?.EmailConfirmed ?? false,
                Email = user?.Email ?? String.Empty
            };

            return model;
        }

        public bool IsUserSubscribed(string username, int courseId)
        {
            return this.context.Subscriptions.Any(s => s.User.UserName == username && s.CourseId == courseId);
        }
    }
}
