﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LearnSoftware.ViewModels.Users
{
    public class SendVerificationEmailViewModel
    {
        public string Email { get; set; }

        public string RedirectUrl { get; set; }
    }
}
