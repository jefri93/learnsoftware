﻿using System;

namespace LearnSoftware.Models.Learn
{
    public class TestResultTestClass
    {
        public int TestResultId { get; set; }
        public TestResult TestResult { get; set; }

        public int TestClassId { get; set; }
        public TestClass TestClass { get; set; }

        public string MachineId { get; set; }

        public DateTime? SentForProcessingAt { get; set; }
    }
}
