﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LoggerLite;
using Microservice.Tools;
using Microservice.Tools.Interfaces;

namespace Teacher.CodeExecuters
{
    public abstract class BaseCodeExecuter : ICodeExecuter
    {
        public BaseCodeExecuter(IMSConfiguration configuration,
            string typeName = "Program",
            string methodName = "Main",
            object[] mainClassContructorParameters = null,
            object[] methodParameters = null)
        {
            this.Configuration = configuration;
            this.TypeName = typeName;
            this.MethodName = methodName;
            this.MainClassContructorParameters = mainClassContructorParameters ?? new object[0];
            this.MethodParameters = methodParameters ?? new object[1] { new string[0] };
        }

        protected IMSConfiguration Configuration { get; }

        protected object[] MainClassContructorParameters { get; private set; }

        protected string TypeName { get; private set; }

        protected string MethodName { get; private set; }

        protected object[] MethodParameters { get; private set; }

        public abstract void Execute(int testResultId, string[] classes, long memoryThreshold);

        public async Task ExecuteAsync(int testResultId, string[] classes, long memoryThreshold)
        {
            this.Execute(testResultId, classes, memoryThreshold);
        }

        protected string GetFullClassName(string typeName, string methodName, string[] classes)
        {
            if(classes.Length > 1)
            {
                return typeName;
            }

            var cSharpClass = classes.FirstOrDefault(c => c.Contains($"class {typeName}") && c.Contains(methodName));

            if (String.IsNullOrEmpty(cSharpClass))
            {
                return typeName;
            }

            Match match = Regex.Match(cSharpClass, @"namespace\s+[^\s\{]+");

            if (String.IsNullOrEmpty(match.ToString()))
            {
                return typeName;
            }

            var namespaceName = match.Value.Split(' ')[1];


            var finalResult = $"{namespaceName}.{typeName}";

            return finalResult;
        }

    }
}
