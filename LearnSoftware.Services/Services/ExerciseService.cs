﻿using System;
using System.Collections.Generic;
using System.Linq;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.ViewModels.Exercises;
using LearnSoftware.Models;
using LearnSoftware.Models.Learn;
using Microsoft.EntityFrameworkCore;
using LearnSoftware.Services.Interfaces;
using LearnSoftware.Services.Dtos;
using Newtonsoft.Json;

namespace LearnSoftware.Services
{
    public class ExerciseService : IExerciseService
    {
        private LSIdentityContext context;
        private IPageManager pageService;
        private static readonly object locker = new object();

        public ExerciseService(LSIdentityContext context, IPageManager pageService)
        {
            this.context = context;
            this.pageService = pageService;
        }

        public Exercise GetExercise(int id)
        {
            return context.Exercises
                .Include(e => e.ExerciseTests)
                .Include(e => e.Lecture)
                .Include(e => e.Lecture.Course)
                .FirstOrDefault(e => e.Id == id);
        }


        public IEnumerable<TestResult> GetTestResults(Exercise exercise, string username, int? page = null)
        {
            if (page < 0)
            {
                return new List<TestResult>();
            }

            IQueryable<TestResult> testResults = context.TestResults
                .Include(tr => tr.TestResultLines)
                   .Where(tr => tr.User.UserName == username && exercise.ExerciseTests
                          .Any(e => e.Id == tr.ExerciseTest.Id)).OrderByDescending(tr => tr.Id);

            if (page != null)
            {
                var skip = pageService.GetSkipCount((int)page);
                var take = pageService.ElementsPerPage;

                var exerciseTestsCount = exercise.ExerciseTests.Count();

                testResults = testResults
                    .Skip(skip * exerciseTestsCount)
                    .Take(take * exerciseTestsCount);
            }

            return testResults.ToList();
        }

        public int GetTestResultsCount(Exercise exercise, string username)
        {
            return context.TestResults
                   .Count(tr => tr.User.UserName == username && exercise.ExerciseTests
                          .Any(e => e.Id == tr.ExerciseTest.Id));
        }

        public void SubmitCode(string code, Exercise exercise, LSIdentityUser user)
        {
            TestClass testClass = new TestClass()
            {
                Class = code
            };

            this.context.TestClasses.Add(testClass);
            foreach (var exerciseTest in exercise.ExerciseTests)
            {
                var testResult = new TestResult() { ExerciseTest = exerciseTest, LinesRead = 0, User = user, WriteCount = 0 };
                this.context.TestResults.Add(testResult);
                var testResulttestClass = new TestResultTestClass() { TestResult = testResult, TestClass = testClass };
                this.context.TestResultsTestClasses.Add(testResulttestClass);
            }

            this.context.SaveChanges();
        }

        public IEnumerable<ExerciseTest> GetExerciseTestsWithIO(Exercise exercise)
        {
            var results = context.ExerciseTests
                   .Include(e => e.TestInputs)
                   .Where(ti => exercise.ExerciseTests.Any(e => e.Id == ti.Id))
                   .ToList();

            return results;
        }

        public int CreateExercise(
            Lecture lecture, 
            string name,
            string description, 
            string exampleInput1, 
            string exampleOutput1,
            string exampleInput2,
            string exampleOutput2,
            List<TestLinesViewModel> testLines)
        {
            List<ExerciseTest> exerciseTests = testLines.Select(et => new ExerciseTest()
            {
                TestInputs = et.TestInputLines.Split(Environment.NewLine).Select(i => new TestInputLine()
                {
                    Text = i
                }).ToList(),
                TestExpectedOutputLines = et.TestExpectedOutputLines
            }).ToList();

            Exercise exercise = new Exercise()
            {
                Name = name,
                Description = description,
                ExampleInput1 = exampleInput1,
                ExampleOutput1 = exampleOutput1,
                ExampleInput2 = exampleInput2,
                ExampleOutput2 = exampleOutput2,
                Lecture = lecture,
                ExerciseTests = exerciseTests
            };

            this.context.Exercises.Add(exercise);
            this.context.SaveChanges();

            return exercise.Id;
        }

        public void UpdateExercise(
            Exercise exercise,
            string name,
            string description,
            string exampleInput1,
            string exampleOutput1,
            string exampleInput2,
            string exampleOutput2,
            List<TestLinesViewModel> testLines)
        {
            var exerciseTests = context.ExerciseTests.Include(et => et.TestInputs)
                                    .Where(et => et.Exercise.Id == exercise.Id).ToList();

            for (int i = 0; i < exerciseTests.Count; i++)
            {
                foreach (var testInput in exerciseTests[i].TestInputs)
                {
                    context.TestInputLines.Remove(testInput);
                }

                var testInputs = testLines[i].TestInputLines
                               .Split(Environment.NewLine)
                               .Select(t => new TestInputLine() { Text = t })
                               .ToList();

                foreach (var testInput in testInputs)
                {
                    exerciseTests[i].TestInputs.Add(testInput);
                }

                exerciseTests[i].TestExpectedOutputLines = testLines[i].TestExpectedOutputLines;
            }

            exercise.Name = name;
            exercise.Description = description;
            exercise.ExampleInput1 = exampleInput1;
            exercise.ExampleOutput1 = exampleOutput1;
            exercise.ExampleInput2 = exampleInput2;
            exercise.ExampleOutput2 = exampleOutput2;

            context.Exercises.Update(exercise);
            context.SaveChanges();
        }

        public int GetTestProcessingCount()
        {
            return context.TestResultsTestClasses
                        .Where(trc => trc.TestResult.Success == null
                        && (trc.MachineId == null
                        || trc.MachineId != null && trc.SentForProcessingAt < DateTime.UtcNow.AddMinutes(-3))).Count();
        }

        public UnprocessedTestsDto GetAllUnprocessedTests(string machineId)
        {
            UnprocessedTestsDto dto = new UnprocessedTestsDto();
            lock (locker)
            {
                try
                {
                    var testResultsTestClasses = context.TestResultsTestClasses
                        .Include(trc => trc.TestClass)
                        .Include(trc => trc.TestResult)
                        .Include(trc => trc.TestResult.ExerciseTest)
                        .Include(trc => trc.TestResult.ExerciseTest.TestInputs)
                        .Where(trc => trc.TestResult.Success == null 
                        && (trc.MachineId == null 
                        || trc.MachineId != null && trc.SentForProcessingAt < DateTime.UtcNow.AddMinutes(-3)))
                        .Take(3);

                    foreach (var testResultTestClass in testResultsTestClasses)
                    {
                        testResultTestClass.MachineId = machineId;
                        testResultTestClass.SentForProcessingAt = DateTime.UtcNow;
                        int testResultId = testResultTestClass.TestResultId;
                        var result = dto.GroupedClasses.FirstOrDefault(gc => gc.Key == testResultId);
                        if (result == null)
                        {
                            result = new MockDictionaryObjectDto<string>()
                            {
                                Key = testResultId,
                            };

                            result.ValueList.Add(testResultTestClass.TestClass.Class);
                            dto.GroupedClasses.Add(result);

                            TestIOContainer linesToRead = new TestIOContainer();
                            linesToRead.TestResultId = testResultId;
                            linesToRead.InputLines.AddRange(testResultTestClass.TestResult.ExerciseTest.TestInputs.Select(ti => ti.Text));
                            linesToRead.ExpectedOutputLines = testResultTestClass.TestResult.ExerciseTest.TestExpectedOutputLines;

                            dto.TestIOContainers.Add(linesToRead);

                            continue;
                        }

                        dto.GroupedClasses.First(gc => gc.Key == testResultId).ValueList.Add(testResultTestClass.TestClass.Class);
                    }

                    context.SaveChanges();
                }
                catch
                {
                    //log error
                }
            }

            return dto;
        }

        public bool UpdateTestResultStatus(UpdateTestResultStatusDto updateTestResultStatusDto)
        {
            var testResult = context.TestResults.FirstOrDefault(tr => tr.Id == updateTestResultStatusDto.TestResultId);

            if(testResult == null)
            {
                return false;
            }

            testResult.Message = updateTestResultStatusDto.Message;
            testResult.Success = updateTestResultStatusDto.Success;

            if (!String.IsNullOrEmpty(updateTestResultStatusDto.Json))
            {
                var lines = JsonConvert.DeserializeObject<List<ResultLineDto>>(updateTestResultStatusDto.Json);

                foreach(var line in lines)
                {
                    testResult.TestResultLines.Add(
                        new TestResultLine() 
                        { 
                            LineNumber = line.Number, 
                            Match = line.Match, 
                            ExpectedResult = line.ExpectedResult, 
                            ActualResult = line.ActualResult 
                        });
                }
            }

            context.SaveChanges();
            return true;
        }
    }
}
