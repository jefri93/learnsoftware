﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LearnSoftware.Services.Services
{
    public class EmailService : IEmailSender
    {
        private SmtpClient client;
        private readonly string emailSender;
        public EmailService(IConfiguration configuration)
        {
            this.emailSender = configuration.GetValue<string>("EmailSender");
            this.ConfigureSmtpClient(configuration);
        }

        private void ConfigureSmtpClient(IConfiguration configuration)
        {
            client = new SmtpClient(configuration.GetValue<string>("EmailSmtp"));
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            client.Port = configuration.GetValue<int>("EmailSmtpPort");
            client.Credentials = new NetworkCredential(configuration.GetValue<string>("EmailUsername"), configuration.GetValue<string>("EmailPassword"));
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            try
            {
                //TODO Check why exception is thrown and fix forgot password
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(emailSender);
                mailMessage.To.Add(email);
                mailMessage.IsBodyHtml = true;
                string assemblyLocation = Assembly.GetExecutingAssembly().GetName().CodeBase.Replace("file:///", String.Empty);
                string currentDictionary = Path.GetDirectoryName(assemblyLocation);
                string path = currentDictionary + "\\wwwroot\\images\\logo-large.png";
                byte[] b = File.ReadAllBytes(path);
                var imageBase64 =  "data:image/png;base64," + Convert.ToBase64String(b);

                mailMessage.Body = $@"<img src=""{imageBase64}"">" + htmlMessage;
                mailMessage.Subject = subject;
                client.Send(mailMessage);
            }
            catch
            {
                //log
            }

            return Task.CompletedTask;
        }
    }
}
