﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LearnSoftware.Services.Dtos
{
    public class UnprocessedTestsDto
    {
        public UnprocessedTestsDto()
        {
            GroupedClasses = new List<MockDictionaryObjectDto<string>>();

            TestIOContainers = new List<TestIOContainer>();
        }
        public List<MockDictionaryObjectDto<string>> GroupedClasses { get; set; }

        public List<TestIOContainer> TestIOContainers { get; set; }
    }

    public class MockDictionaryObjectDto<T>
    {
        public MockDictionaryObjectDto()
        {
            ValueList = new List<T>();
        }

        public int Key { get; set; }

        public List<T> ValueList { get; set; }
    }

    public class TestIOContainer
    {
        public TestIOContainer()
        {
            InputLines = new List<string>();
            OutputLines = String.Empty;
        }

        public int TestResultId { get; set; }

        public int LinesRead { get; set; }

        public List<string> InputLines { get; set; }

        public string ExpectedOutputLines { get; set; }

        public string OutputLines { get; set; }
    }
}
