﻿using Microservice.Tools;
using Microservice.Tools.Sevices;
using System;
using Teacher.Dtos;

namespace CompileClassResources.ClassResources._NewUnhit.nonono
{
    public static class ConsoleInterceptor
    {
        private static void WriteJson(int testResultId, bool isNewLine, string text)
        {
            if(text.Length > int.Parse(Config.GetElement("MaxConsoleStringOutputLength")))
            {
                throw new Exception("WriteLine Length cannot be more than 500 Charachters.");
            }

            if(isNewLine)
            {
                text += Environment.NewLine;
            }

            var testIOContainer = FileService.Read<TestIOContainer>(testResultId);
            testIOContainer.OutputLines += text;

            FileService.Write(testResultId, testIOContainer);
        }

        private static string ReadLineJson(int testResultId)
        {
            var linesToRead = FileService.Read<TestIOContainer>(testResultId);
            string line = linesToRead.InputLines[linesToRead.LinesRead++];
            FileService.Write(testResultId, linesToRead);

            return line;
        }

        public static int Read(int testResultId)
        {
            string result = ReadLineJson(testResultId);

            if (result.Length == 0)
            {
                throw new Exception("Cannot read from empty string.");
            }

            return ReadLineJson(testResultId)[0];
        }

        public static ConsoleKeyInfo ReadKey(int testResultId)
        {
            string input = ReadLineJson(testResultId); ;

            if (input.Length == 0)
            {
                input = " ";
            }

            return new ConsoleKeyInfo(input[0], (ConsoleKey)((int)input[0]), false, false, false);
        }

        public static string ReadLine(int testResultId)
        {
            return ReadLineJson(testResultId);
        }

        public static void WriteLine(int testResultId, ulong value)
        {
            WriteJson(testResultId, true, value + "");
        }

        public static void WriteLine(int testResultId, string format, params object[] arg)
        {
            WriteJson(testResultId, true, String.Format(format, arg));
        }
        public static void WriteLine(int testResultId)
        {
            WriteJson(testResultId, true, String.Empty);
        }

        public static void WriteLine(int testResultId, bool value)
        {
            WriteJson(testResultId, true, value + "");
        }

        public static void WriteLine(int testResultId, char[] buffer)
        {
            WriteJson(testResultId, true, String.Join(String.Empty, buffer));
        }

        public static void WriteLine(int testResultId, char[] buffer, int index, int count)
        {
            WriteJson(testResultId, true, String.Join(String.Empty, buffer).Substring(index, count));
        }
        public static void WriteLine(int testResultId, decimal value)
        {
            WriteJson(testResultId, true, value + "");
        }
        public static void WriteLine(int testResultId, double value)
        {
            WriteJson(testResultId, true, value + "");
        }

        [CLSCompliant(false)]
        public static void WriteLine(int testResultId, uint value)
        {
            WriteJson(testResultId, true, value + "");
        }

        public static void WriteLine(int testResultId, int value)
        {
            WriteJson(testResultId, true, value + "");
        }

        public static void WriteLine(int testResultId, object value)
        {
            WriteJson(testResultId, true, value.ToString());
        }

        public static void WriteLine(int testResultId, float value)
        {
            WriteJson(testResultId, true, value + "");
        }

        public static void WriteLine(int testResultId, string value)
        {
            WriteJson(testResultId, true, value);
        }

        public static void WriteLine(int testResultId, long value)
        {
            WriteJson(testResultId, true, value + "");
        }

        public static void WriteLine(int testResultId, char value)
        {
            WriteJson(testResultId, true, value + "");
        }

        public static void Write(int testResultId, ulong value)
        {
            WriteJson(testResultId, false, value + "");
        }

        public static void Write(int testResultId, string format, params object[] arg)
        {
            WriteJson(testResultId, false, String.Format(format, arg));
        }

        public static void Write(int testResultId, bool value)
        {
            WriteJson(testResultId, false, value + "");
        }

        public static void Write(int testResultId, char[] buffer)
        {
            WriteJson(testResultId, false, String.Join(String.Empty, buffer));
        }

        public static void Write(int testResultId, char[] buffer, int index, int count)
        {
            WriteJson(testResultId, false, String.Join(String.Empty, buffer).Substring(index, count));
        }
        public static void Write(int testResultId, decimal value)
        {
            WriteJson(testResultId, false, value + "");
        }
        public static void Write(int testResultId, double value)
        {
            WriteJson(testResultId, false, value + "");
        }

        [CLSCompliant(false)]
        public static void Write(int testResultId, uint value)
        {
            WriteJson(testResultId, false, value + "");
        }

        public static void Write(int testResultId, int value)
        {
            WriteJson(testResultId, false, value + "");
        }

        public static void Write(int testResultId, object value)
        {
            WriteJson(testResultId, false, value.ToString());
        }

        public static void Write(int testResultId, float value)
        {
            WriteJson(testResultId, false, value + "");
        }

        public static void Write(int testResultId, string value)
        {
            WriteJson(testResultId, false, value);
        }

        public static void Write(int testResultId, long value)
        {
            WriteJson(testResultId, false, value + "");
        }

        public static void Write(int testResultId, char value)
        {
            WriteJson(testResultId, false, value + "");
        }
    }
}
