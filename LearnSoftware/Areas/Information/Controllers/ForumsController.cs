﻿using System;
using System.Collections.Generic;
using System.Linq;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.ViewModels.ForumCategories;
using LearnSoftware.ViewModels.Forums;
using LearnSoftware.Controllers;
using LearnSoftware.Models;
using LearnSoftware.Models.Information;
using LearnSoftware.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LearnSoftware.Services.Interfaces;

namespace LearnSoftware.Areas.Information.Controllers
{
    [Area("Information")]
    public class ForumsController : Controller
    {
        private IPageManager pageService;
        private IForumService forumService;
        private IForumCategoryService forumCategoryService;
        private IForumMessageService forumMessageService;
        private UserManager<LSIdentityUser> userManager;

        public ForumsController(IForumService forumService, IForumCategoryService forumCategoryService, IForumMessageService forumMessageService, UserManager<LSIdentityUser> userManager, IPageManager pageManager)
        {
            this.forumCategoryService = forumCategoryService;
            this.forumMessageService = forumMessageService;
            this.forumService = forumService;
            this.pageService = pageManager;
            this.userManager = userManager;
        }

        [Authorize]
        public IActionResult Index(int forumCategoryId, string search = null, int page = 1)
        {
            var forumCategory = forumCategoryService.GetForumCategory((int)forumCategoryId);

            if(forumCategory == null)
            {
                return this.NotFound();
            }

            var forums = forumService.GetForums(forumCategoryId, page, search)
                .Select(f => new FocumCategoryIndexForumViewModel()
                {
                    Id = f.Id,
                    Name = f.Name,
                    CreatedOn = f.CreationDate.ToString(),
                    CreatorUsername = f.Creator.UserName
                });

            var maxPage = pageService.GetMaxPage(forumService.GetCount(forumCategoryId));
            var minPage = pageService.GetMinPage(maxPage);

            ForumCategoryIndexViewModel model = new ForumCategoryIndexViewModel()
            {
                Id = (int)forumCategoryId,
                CurrentPage  = page,
                Search = search,
                MinPage = minPage,
                MaxPage = maxPage,
                Forums = forums.ToList(),
                ForumCategoryName = forumCategory.Name
            };

            return View(model);
        }

        [Authorize]
        public IActionResult RedirectToForum(int forumMessageId)
        {
            var message = forumMessageService.GetForumMessage((int)forumMessageId);

            if (message == null)
            {
                return this.NotFound();
            }

            return this.RedirectToAction("Forum", "Forums", new { id = message.Forum.Id });
        }

        [Authorize]
        [HttpGet("/Forums/Create/{forumCategoryId}")]
        public IActionResult Create(int forumCategoryId)
        {
            var hasForumCategory = forumCategoryService.HasForumCategory((int)forumCategoryId);

            if(!hasForumCategory)
            {
                return this.NotFound();
            }

            CreateForumViewModel model = new CreateForumViewModel() { Id = (int)forumCategoryId };

            return this.View(model);
        }

        [Authorize]
        [HttpPost("/Forums/Create/{forumCategoryId}")]
        public IActionResult Create(CreateForumViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var forumCategory = this.forumCategoryService.GetForumCategory(model.Id);

            if (forumCategory == null)
            {
                return this.NotFound();
            }

            var user = this.userManager.FindByNameAsync(this.User.Identity.Name).Result;

            if(user == null)
            {
                return this.NotFound();
            }

            int forumId = forumService.AddForum(user, forumCategory, model.Name, model.FirstComment);

            return this.RedirectToAction("Forum","Forums",new { id = forumId });
        }

        [Authorize]
        public IActionResult Forum(int id, int page = 1)
        {
            var forum = this.forumService.GetForum((int)id);

            if(forum == null)
            {
                return this.NotFound();
            }

            IEnumerable<ForumMessage> messages = forumMessageService.GetForumMessages(id, page)
                                                                    .OrderBy(m => m.CreationDate);
            var maxPage = pageService.GetMaxPage(forumMessageService.GetCount(id));
            var minPage = pageService.GetMinPage(maxPage);

            var model = new ForumViewModel()
            {
                Id = forum.Id,
                IsActive = forum.IsActive,
                CurrentPage = page,
                MinPage = minPage,
                MaxPage = maxPage,
                CreationDate = forum.CreationDate.ToString(),
                UpdateDate = forum.UpdateDate.ToString(),
                Name = forum.Name,
                CreatorUsername = forum.Creator.UserName,
                Messages = messages.Select(m => new ForumMessageViewModel()
                {
                    CommenterUsername = m.User.UserName,
                    CreationDate = m.CreationDate.ToString(),
                    UpdateDate = m.UpdateDate.ToString(),
                    Id = m.Id,
                    Message = m.Message
                }).ToList()
            };

            return this.View(model);
        }

        [Authorize]
        public IActionResult DeactivateForum(int forumId)
        {
            var forum = forumService.GetForum((int)forumId);

            if(forum == null)
            {
                return this.NotFound();
            }

            if(this.User.Identity.Name != forum.Creator.UserName)
            {
                return this.Unauthorized();
            }

            forumService.DeactivateForum(forum);

            return this.RedirectToAction("Forum", "Forums", new { id = forumId });
        }
    }
}