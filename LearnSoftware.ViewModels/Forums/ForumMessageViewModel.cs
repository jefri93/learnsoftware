﻿using System;
namespace LearnSoftware.ViewModels.Forums
{
    public class ForumMessageViewModel
    {
        public int Id { get; set; }

        public string CommenterUsername { get; set; }

        public string CreationDate { get; set; }

        public string UpdateDate { get; set; }

        public string Message { get; set; }
    }
}
