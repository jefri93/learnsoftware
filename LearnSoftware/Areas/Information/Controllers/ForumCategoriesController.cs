﻿using System;
using System.Collections.Generic;
using System.Linq;
using LearnSoftware.ViewModels.ForumCategories;
using LearnSoftware.Controllers;
using LearnSoftware.Models;
using LearnSoftware.Models.Information;
using LearnSoftware.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LearnSoftware.Services.Interfaces;

namespace LearnSoftware.Areas.Information.Controllers
{
    [Area("Information")]
    public class ForumCategoriesController : Controller
    {
        private IPageManager pageManager;
        private IForumCategoryService forumCategoryService;
        private IForumService forumService;

        public ForumCategoriesController(IForumCategoryService forumCategoryService, IForumService forumService, IPageManager pageManager)
        {
            this.pageManager = pageManager;
            this.forumCategoryService = forumCategoryService;
            this.forumService = forumService;
        }

        [Authorize]
        public IActionResult Index(string search, int page = 1)
        {
            List<ForumCategoryViewModel> forumCategories = forumCategoryService.GetForumCategories()
                .Select(fc => new ForumCategoryViewModel() { Id = fc.Id, Name = fc.Name }).ToList();

            var maxPage = pageManager.GetMaxPage(forumService.GetCount());
            var minPage = pageManager.GetMinPage(maxPage);
            var forums = forumService.GetForums(null, page, search)
                .Select(f => new FocumCategoryIndexForumViewModel()
                {
                    Id = f.Id,
                    Name = f.Name,
                    CreatedOn = f.CreationDate.ToString(),
                    CreatorUsername = f.Creator.UserName
                });

            ForumCategoryIndexViewModel forumCategoryContainer = new ForumCategoryIndexViewModel()
            {
                CurrentPage = page,
                Search = search,
                MinPage = minPage,
                MaxPage = maxPage,
                Forums = forums.ToList(),
            };

            ForumCategoryIndexContainerViewModel model = new ForumCategoryIndexContainerViewModel()
            {
                ForumCategoryViewModel = forumCategoryContainer,
                ForumCategories = forumCategories
            };

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult Create(string name)
        {
            var error = forumCategoryService.ValidateCategoryName(name);

            if (String.IsNullOrEmpty(error))
            {
                forumCategoryService.CreateForumCategory(name);
            }

            return this.RedirectToAction("Index","ForumCategories");
        }
    }
}