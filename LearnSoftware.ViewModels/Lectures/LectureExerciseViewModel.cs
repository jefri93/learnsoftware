﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LearnSoftware.Models;

namespace LearnSoftware.ViewModels.Lectures
{
    public class LectureExerciseViewModel
    {
        public int Id { get; set; }

        public CourseType CourseType { get; set; }

        public string Name { get; set; }
    }
}
