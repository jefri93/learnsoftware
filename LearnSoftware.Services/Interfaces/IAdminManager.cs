﻿using System;
namespace LearnSoftware.Services.Interfaces
{
    public interface IAdminManager
    {
        void CreateRoles();

        bool CreateDefaultAdmin();

        bool SetUserRoleUser(string username);

        bool SetUserRoleAdmin(string username);
    }
}
