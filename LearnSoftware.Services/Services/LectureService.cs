﻿using System;
using System.Collections.Generic;
using System.Linq;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using LearnSoftware.Models.Learn;
using LearnSoftware.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LearnSoftware.Services
{
    public class LectureService : ILectureService
    {
        private LSIdentityContext context;

        public LectureService(LSIdentityContext context)
        {
            this.context = context;
        }

        public IEnumerable<Lecture> GetLectures(int courseId)
        {
            var lectures = context.Lectures
                                  .Where(l => l.Course.Id == courseId);

            return lectures.ToList();
        }

        public Lecture GetLecture(int id)
        {
            return context.Lectures
                   .Include(l => l.Exercices)
                   .Include(l => l.Course)
                   .FirstOrDefault(l => l.Id == id);
        }

        public int CreateLecture(Course course, string textExplnation, string videoUrl, string name, int videoLengthInMinutes)
        {
            
            Lecture lecture = new Lecture()
            {
                Name = name,
                LectureTextExplnation = textExplnation,
                LectureVideoUrl = videoUrl,
                Course = course,
                VideoLength = new TimeSpan(0, videoLengthInMinutes, 0).Ticks
            };

            context.Lectures.Add(lecture);
            context.SaveChanges();

            return lecture.Id;
        }

        public void UpdateLecture(Lecture lecture, string textExplnation, string videoUrl, string name, int videoLengthInMinutes)
        {
            lecture.LectureTextExplnation = textExplnation;
            lecture.LectureVideoUrl = videoUrl;
            lecture.Name = name;
            lecture.VideoLength = new TimeSpan(0, videoLengthInMinutes, 0).Ticks;
            
            this.context.Lectures.Update(lecture);
            this.context.SaveChanges();
        }

        public bool HasLecture(int lectureId)
        {
            return context.Lectures.Any(l => l.Id == lectureId);
        }
    }
}
