﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LearnSoftware.Models;

namespace LearnSoftware.ViewModels.Exercises
{
    public class ExerciseExerciseViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ExampleInput1 { get; set; }

        public string ExampleOutput1 { get; set; }

        public string ExampleInput2 { get; set; }

        public string ExampleOutput2 { get; set; }

        public CourseType CourseType { get; set; }

        public int ExerciseTestCount { get; set; }

        [Required]
        [StringLength(int.MaxValue, MinimumLength = 20, ErrorMessage = "You need to have atleast 20 chars to make a submission.")]
        public string Code { get; set; }

        public List<ExerciseTestResultViewModel> ExerciseTestResults { get; set; }

        public int MinPage { get; set; }

        public int MaxPage { get; set; }
    }
}
