﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models;
using LearnSoftware.Models.Learn;
using LearnSoftware.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace LearnSoftware.Services
{
    public class CourseService : ICourseService
    {
        private LSIdentityContext context;

        public CourseService(LSIdentityContext context)
        {
            this.context = context;
        }


        public void ChangeCourseStatus(Course course, bool? isActive = null)
        {
            course.IsActive = isActive ?? (course.IsActive ? false : true);

            context.Courses.Update(course);
            context.SaveChanges();
        }

        public IEnumerable<Course> GetCourses(string search = null, bool isAdmin = false)
        {
            IEnumerable<Course> coursesQuery = context.Courses
                .Include(c => c.Lectures)
                .Include(c => c.UsersCourses)
                .Where(c => c.IsActive || isAdmin);

            if (!String.IsNullOrEmpty(search))
            {
                coursesQuery = coursesQuery.Where(c => c.Name.ToLower().Contains(search.ToLower()));
            }

            return coursesQuery.ToList();
        }

        public IEnumerable<Course> GetCoursesInProgress(string username, string search = null, bool isAdmin = false)
        {
            IEnumerable<Course> coursesQuery = context.Courses
                .Include(c => c.Lectures)
                .Include(c => c.UsersCourses)
                .Where(c => c.UsersCourses
                       .Any(uc => uc.User.UserName == username)
                       && (c.IsActive || isAdmin));

            if (!String.IsNullOrEmpty(search))
            {
                coursesQuery = coursesQuery.Where(c => c.Name.ToLower().Contains(search.ToLower()));
            }

            return coursesQuery.ToList();
        }

        public int CreateCourse(CourseType courseType, string name, string description, decimal price)
        {
            Course course = new Course()
            {
                CourseType = (CourseType)courseType,
                Description = description,
                IsActive = false,
                Name = name,
                CoursePrice = Math.Round(price, 2)
        };

            context.Courses.Add(course);
            context.SaveChanges();

            return course.Id;
        }

        public void UpdateCourse(Course course, CourseType courseType, string name, string description, decimal price)
        {
            course.CourseType = courseType;
            course.Description = description;
            course.Name = name;
            course.CoursePrice = Math.Round(price,2);

            context.Courses.Update(course);
            context.SaveChanges();
        }

        public Course GetCourse(int courseId)
        {
            return context.Courses.FirstOrDefault(c => c.Id == courseId);
        }

        public bool HasCourse(int courseId)
        {
            return context.Courses.Any(c => c.Id == courseId);
        }

        public bool HasUserCourse(string userId, int courseId)
        {
           return this.context.UsersCourses
                 .Any(uc => uc.UserId == userId && uc.CourseId == courseId);
        }

        public void CreateUserCourse(Course course, LSIdentityUser user)
        {
            UserCourse userCourse = new UserCourse()
            {
                Course = course,
                User = user
            };

            this.context.UsersCourses.Add(userCourse);
            this.context.SaveChanges();
        }

        public bool IsCourseFree(int courseId)
        {
            return context.Courses.Any(c => c.Id == courseId && c.CourseType == CourseType.Free);
        }

        public IEnumerable<Course> GetPurchasedCourses(string username, string search, bool isAdmin)
        {
            var user = context.Users.FirstOrDefault(u => u.UserName == username);

            if(user == null)
            {
                return new List<Course>();
            }

            IEnumerable<Course> coursesQuery = context.Subscriptions
                .Include(s => s.Course)
                .Include(s => s.Course.Lectures)
                .Where(s => s.UserId == user.Id && (s.Course.IsActive || isAdmin))
                .Select(s => s.Course);

            if (!String.IsNullOrEmpty(search))
            {
                coursesQuery = coursesQuery.Where(c => c.Name.ToLower().Contains(search.ToLower()));
            }

            return coursesQuery.ToList();
        }

        public bool IsCoursePurchased(int courseId, string username)
        {
            return context.Users.Include(u => u.Subscriptions).FirstOrDefault(u => u.UserName == username)?.Subscriptions?.Any(s => s.CourseId == courseId) ?? false;
        }
    }
}
