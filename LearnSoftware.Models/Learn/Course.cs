﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LearnSoftware.Models.Learn
{
    public class Course
    {
        public Course()
        {
            Lectures = new List<Lecture>();
            UsersCourses = new List<UserCourse>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public CourseType CourseType { get; set; }

        public decimal CoursePrice { get; set; }

        [Required]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public ICollection<Lecture> Lectures { get; set; }

        public ICollection<UserCourse> UsersCourses { get; set; }

    }
}
