﻿using System;
using System.Xml;
using Microservice.Tools.Helpers;

namespace Microservice.Tools
{
    public static class Config
    {
        public static string GetElement(string key)
        {
            XmlDocument doc = new XmlDocument();

            string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string configName = "app.config";
            char slash = DirectoryHelper.GetSlash();

            doc.Load(directory.EndsWith(slash) ? directory + configName : directory + slash + configName);
            string value = doc.DocumentElement.SelectSingleNode($"/configuration/appSettings/add[@key='{key}']").Attributes["value"].Value;

            return value;
        }
    }
}
