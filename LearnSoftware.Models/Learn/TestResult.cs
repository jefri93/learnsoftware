﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LearnSoftware.Areas.Identity.Data;

namespace LearnSoftware.Models.Learn
{
    public class TestResult
    {
        public TestResult()
        {
            TestResultsTestClasses = new List<TestResultTestClass>();
            TestResultLines = new List<TestResultLine>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public int LinesRead { get; set; }

        [Required]
        public int WriteCount { get; set; }

        public string TestOutputLines { get; set; }

        public ICollection<TestResultTestClass> TestResultsTestClasses { get; set; }

        public ICollection<TestResultLine> TestResultLines { get; set; }

        public bool? Success { get; set; }

        public string Message { get; set; }

        public ExerciseTest ExerciseTest { get; set; }

        public LSIdentityUser User { get; set; }
    }
}
