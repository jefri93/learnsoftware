﻿using System;
using System.Collections.Generic;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.Models.Information;

namespace LearnSoftware.Services.Interfaces
{
    public interface IForumService
    {
        IEnumerable<Forum> GetForums(int? forumCategoryId = null, int? page = null, string search = null, string commenterUserId = null);

        int GetCount(int? forumCategoryId = null);

        Forum GetForum(int id);

        int AddForum(LSIdentityUser user, ForumCategory forumCategory, string forumName, string firstComment);

        void DeactivateForum(Forum forum);
    }
}
