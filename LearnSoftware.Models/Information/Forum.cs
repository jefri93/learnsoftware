﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LearnSoftware.Areas.Identity.Data;

namespace LearnSoftware.Models.Information
{
    public class Forum
    {
        public Forum()
        {
            Messages = new List<ForumMessage>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public ForumCategory ForumCategory { get; set; }

        public ICollection<ForumMessage> Messages { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public DateTime UpdateDate { get; set; }

        [Required]
        public LSIdentityUser Creator { get; set; }

        [Required]
        public bool IsActive { get; set; }
    }
}
