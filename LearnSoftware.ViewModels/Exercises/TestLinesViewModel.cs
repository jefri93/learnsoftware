﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace LearnSoftware.ViewModels.Exercises
{
    [Serializable]
    public class TestLinesViewModel
    {
        public string TestInputLines { get; set; }

        public string TestExpectedOutputLines { get; set; }
    }
}
