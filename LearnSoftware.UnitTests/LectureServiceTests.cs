﻿using System;
using System.Linq;
using LearnSoftware.Services;
using LearnSoftware.Services.Interfaces;
using Xunit;

namespace LearnSoftware.UnitTests
{
    public class LectureServiceTests
    {
        private ICourseService courseService;
        private ILectureService lectureService;

        public LectureServiceTests()
        {
            var context = TestData.GetDataFilledContext();
            courseService = new CourseService(context);
            lectureService = new LectureService(context);
        }

        [Fact]
        public void GetLecturesTest()
        {
            var lectures = lectureService.GetLectures(6);

            Assert.True(2 == lectures.Count());
        }

        [Fact]
        public void HasLectureTest()
        {
            var result = lectureService.HasLecture(6);

            Assert.True(result);
        }

        [Fact]
        public void CreateLectureTest()
        {
            var course = courseService.GetCourse(7);
            var lectureId = lectureService.CreateLecture(course,"Explain", "", "Create Lecture", 0);

            Assert.True(lectureId > 0);
        }

        [Fact]
        public void UpdateLectureTest()
        {
            var course = courseService.GetCourse(7);
            var lectureId = lectureService.CreateLecture(course, "Explain", "", "Create Lecture", 0);
            var lecture = lectureService.GetLecture(lectureId);
            lectureService.UpdateLecture(lecture, "Expl", "", "Update Lecture", 0);

            Assert.True(lecture.Name == "Update Lecture");
        }
    }
}
