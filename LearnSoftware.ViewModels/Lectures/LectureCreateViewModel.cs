﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LearnSoftware.ViewModels.Lectures
{
    public class LectureCreateViewModel
    {
        [Required]
        public int Id { get; set; }

        [Url]
        public string LectureVideoUrl { get; set; }

        public int LectureVideoLengthInMinutes { get; set; }

        [Required]
        public string Name { get; set; }

        public string LectureTextExplnation { get; set; }
    }
}
