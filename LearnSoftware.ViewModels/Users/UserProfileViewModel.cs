﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearnSoftware.ViewModels
{
    public class UserProfileViewModel
    {
        public string UserName { get; set; }

        public Dictionary<int, string> ViewedCourses { get; set; }

        public Dictionary<int, string> CreatedForums { get; set; }

        public Dictionary<int, string> ForumMessages { get; set; }

        public int ResultSuccessCount { get; set; }

        public int ResultFailedCount { get; set; }

        public int ResultTotalCount { get; set; }

        public bool IsAdmin { get; set; }
    }
}
