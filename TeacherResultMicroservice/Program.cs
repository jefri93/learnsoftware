﻿using System;
using System.Linq;
using Microservice.Tools.Interfaces;
using Microservices;
using Microservices.Microservices;
using Teacher.CodeExecuters;
using Teacher.ResultComaprers;
using System.Configuration;
using System.Xml;
using Microservice.Tools;
using LoggerLite;
using Microsoft.Extensions.DependencyInjection;
using Microservice.Tools.Services;
using System.Reflection;
using System.IO;

namespace TeacherResultMicroservice
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection injector = new ServiceCollection();

            string[] forbiddenKeywords = Config.GetElement("ForbiddenKeywords").Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
            int timeIntervalMs = int.Parse(Config.GetElement("TimeIntervalMs"));
            string logPath = Config.GetElement("LogFile");
            int timeout = int.Parse(Config.GetElement("Timeout"));
            int codeBlockByteLimit = int.Parse(Config.GetElement("CodeBlockByteLimit"));
            int testResultProcessingThreadCount = int.Parse(Config.GetElement("TestResultProcessingThreadCount"));
            int maxProcessedCount = int.Parse(Config.GetElement("MaxProcessedCount"));
            long memoryThreshold = long.Parse(Config.GetElement("MemoryThreshold"));
            string learnSoftwareTestsApiUrl = Config.GetElement("LearnSoftwareTestsApiUrl");
            string learnSoftwareTestsApiUsername = Config.GetElement("LearnSoftwareTestsApiUsername");
            string learnSoftwareTestsApiPassword = Config.GetElement("LearnSoftwareTestsApiPassword");

            IMSConfiguration msConfig = new MSConfiguration();
            msConfig.ForbiddenKeywords = forbiddenKeywords;
            msConfig.TimeIntervalMs = timeIntervalMs;
            msConfig.Timeout = timeout;
            msConfig.CodeBlockByteLimit = codeBlockByteLimit;
            msConfig.TestResultProcessingThreadCount = testResultProcessingThreadCount;
            msConfig.MaxProcessedCount = maxProcessedCount;
            msConfig.MemoryThreshold = memoryThreshold;

            injector.AddSingleton(new LearnSoftwareExerciseTestsService(learnSoftwareTestsApiUrl, learnSoftwareTestsApiUsername, learnSoftwareTestsApiPassword));
            injector.AddSingleton<IMSConfiguration>(msConfig);
            string assemblyLocation = Assembly.GetExecutingAssembly().GetName().CodeBase.Replace("file:///", String.Empty);
            string currentDictionary = Path.GetDirectoryName(assemblyLocation) + $"\\Logging\\{DateTime.UtcNow.ToString("MM-dd-yyyy")}\\";
            Directory.CreateDirectory(currentDictionary);
            injector.AddSingleton<ILogger>(new JsonFileLogger(currentDictionary + "Logs.txt"));
            injector.AddSingleton(typeof(IResultComparer), typeof(ResultComparer));
            injector.AddSingleton(typeof(BaseCodeExecuter), typeof(CodeExecuter));
            injector.AddSingleton(typeof(IMicroservice), typeof(ApplicationProcessingMicroservice));

            var serviceProvider = injector.BuildServiceProvider();

            IMicroservice microservice = serviceProvider.GetService<IMicroservice>();
            microservice.Run();
        }
    }
}




