﻿using System;
using System.Collections.Generic;

namespace LearnSoftware.ViewModels.Courses
{
    public class CourseCourseViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string CourseType { get; set; }

        public bool IsActive { get; set; }

        public decimal Price { get; set; }

        public bool IsPurchased { get; set; }

        public List<CourseLectureViewModel> Lectures { get; set; }
    }
}
