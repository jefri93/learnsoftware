﻿using System;
using LearnSoftware.Areas.Identity.Data;

namespace LearnSoftware.Models.Learn
{
    public class UserCourse
    {
        public string UserId { get; set; }
        public LSIdentityUser User { get; set; }

        public int CourseId { get; set; }
        public Course Course { get; set; }

        public DateTime LastViewedDate { get; set;}
    }
}
