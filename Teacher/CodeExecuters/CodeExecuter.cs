﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using CompileClassResources.ClassResources._NewUnhit.nonono;
using LoggerLite;
using Microservice.Tools.Interfaces;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Teacher.Exceptions;

namespace Teacher.CodeExecuters
{
    public class CodeExecuter : BaseCodeExecuter
    {
        private static Random random;

        public CodeExecuter(
            IMSConfiguration configuration,
            ILogger logger,
            string typeName = "Program", 
            string methodName = "Main",
            object[] mainClassContructorParameters = null, 
            object[] methodParameters = null) 
            : base(configuration, typeName, methodName, mainClassContructorParameters, methodParameters)
        {
            random = new Random();

            string assemblyPath = Directory.GetCurrentDirectory() + "GeneratedAssemblies\\";
            Directory.CreateDirectory(assemblyPath);
        }

        private IReadOnlyList<string> classNameWithExtensions => new List<string>()
        {
             "System.Console",
             "Console" 
        };

        public override void Execute(int testResultId, string[] classes, long memoryThreshold)
        {
            Type interceptorType = typeof(ConsoleInterceptor);
            string[] ClassesWithoutEmptyEntries = classes.Where(c => !String.IsNullOrWhiteSpace(c) && !String.IsNullOrWhiteSpace(c)).ToArray();

            if (ClassesWithoutEmptyEntries.Length == 0)
            {
                throw new TeacherException($"No classes to compile.");
            }

            string[] injectedClasses = GetClassesWithInjectedInterceptor(testResultId, ClassesWithoutEmptyEntries, interceptorType);

            Type[] additionalTypes = new Type[1] { interceptorType };
            
            var codeType = GetCompiledType(injectedClasses, this.TypeName, additionalTypes);
            var method = codeType.GetMethod(this.MethodName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            Task task = null;

            if (codeType.IsAbstract && codeType.IsSealed || method.IsStatic)
            {
                task = new Task(()=>method.Invoke(null, this.MethodParameters));
            }
            else
            {
                var instance = Activator.CreateInstance(codeType, this.MainClassContructorParameters);
                task = new Task(() => method.Invoke(instance, this.MethodParameters));
            }

            var cts = new CancellationTokenSource();
            try
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                task.Start();

                Thread.Sleep(100);
                long memoryAfter = Process.GetCurrentProcess().VirtualMemorySize64;

                if (memoryAfter > memoryThreshold)
                {
                    cts.Cancel();
                    throw new TeacherException($"Execution went above memory threshold. Please retry you submission.");
                }

                if (!task.Wait(Configuration.Timeout, cts.Token))
                {
                    cts.Cancel();
                    throw new TeacherException("Execution timed out.");
                }
            }
            catch (ObjectDisposedException e)
            {
                throw new TeacherException($"Execution for TestResultId {testResultId} timed out.");
            }
        }
       

        private void ValidateNamespaces(string[] classes)
        {
            int currentNamespacesCount = 0;

            for (int i = 0; i < classes.Length; i++)
            {
                string cSharpClass = classes[i];
                var currentClassNamespaces = Regex.Matches(cSharpClass, @"namespace\s+[^\s\{]+");

                currentNamespacesCount += currentClassNamespaces.Count();
            }

            if (currentNamespacesCount > 1)
            {
                throw new TeacherException("Compilation error. Please remove all namespaces.");
            }
        }

        private string[] GetClassesWithInjectedInterceptor(int testResultId,string[] classes, Type interceptorType)
        {
            string[] resultClasses = new string[classes.Length];

            var methodNames = interceptorType.GetMethods(
                BindingFlags.Public |
                BindingFlags.Static)
                .GroupBy(m => m.Name)
                .Select(g => g.Key);

            string interceptorName = interceptorType.FullName;

            for (int i = 0; i < classes.Length; i++)
            {
                resultClasses[i] = classes[i];

                foreach (var className in this.classNameWithExtensions)
                {
                    foreach (var name in methodNames)
                    {
                        resultClasses[i] = resultClasses[i].Replace($"{className}.{name}()", $"{interceptorName}.{name}#({testResultId})");
                        resultClasses[i] = resultClasses[i].Replace($"{className}.{name}(", $"{interceptorName}.{name}({testResultId}, ");
                        resultClasses[i] = resultClasses[i].Replace($"{interceptorName}.{name}#", $"{interceptorName}.{name}");
                    }
                }
            }
            
            return resultClasses;
        }

        private CSharpCompilation GetCustomCompilation(Type[] additionalTypes)
        {
            var compilation = CSharpCompilation.Create(Path.GetRandomFileName() + ".dll")
                   .WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
                   .AddReferences(MetadataReference.CreateFromFile(Assembly.Load(new AssemblyName("mscorlib")).Location))
                   .AddReferences(MetadataReference.CreateFromFile(typeof(Enumerable).GetTypeInfo().Assembly.Location))
                   .AddReferences(MetadataReference.CreateFromFile(typeof(StringBuilder).GetTypeInfo().Assembly.Location))
                   .AddReferences(MetadataReference.CreateFromFile(typeof(Regex).GetTypeInfo().Assembly.Location))
                   .AddReferences(MetadataReference.CreateFromFile(typeof(IEnumerable<>).GetTypeInfo().Assembly.Location))
                   .AddReferences(MetadataReference.CreateFromFile(Assembly.Load("System.Collections, Version=4.1.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a").Location))
                   .AddReferences(MetadataReference.CreateFromFile(Assembly.Load("System.Runtime, Version=4.2.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a").Location));

            if (additionalTypes != null)
            {
                foreach (var type in additionalTypes)
                {
                    compilation = compilation.AddReferences(MetadataReference.CreateFromFile(type.GetTypeInfo().Assembly.Location));
                }
            }

            return compilation;
        }

        private void ValidateClassesForForbiddenKeywords(string[] cSharpClasses)
        {
            foreach (var forbiddenNamespace in Configuration.ForbiddenKeywords)
            {
                if (cSharpClasses.Any(c => c.ToLower().Contains(forbiddenNamespace.ToLower())))
                {
                    throw new TeacherException($"Compilation failed. Namespace {forbiddenNamespace} not allowed in this compiler.");
                }
            }
        }


        private Type GetCompiledType(string[] cSharpClasses, string typeName, Type[] additionalTypes = null)
        {
            ValidateCodeSize(cSharpClasses);
            ValidateNamespaces(cSharpClasses);
            ValidateClassesForForbiddenKeywords(cSharpClasses);

            var compilation = GetCustomCompilation(additionalTypes);

            foreach (var cSharpClass in cSharpClasses)
            {
                compilation = compilation.AddSyntaxTrees(CSharpSyntaxTree.ParseText(cSharpClass));
            }

            using (var ms = new MemoryStream())
            {
                var result = compilation.Emit(ms);
                if (!result.Success)
                {
                    var errors = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);
                    StringBuilder builder = new StringBuilder();

                    foreach (var diagnostic in errors)
                    {
                        builder.AppendLine(String.Format("{0}: {1}", diagnostic.Id, diagnostic.GetMessage()));
                    }

                    throw new Exception(builder.ToString());
                }

                ms.Seek(0, SeekOrigin.Begin);

                var assembly = Assembly.Load(ms.ToArray());
                //var assembly = context.LoadFromStream(ms);
                string fullName = GetFullClassName(typeName, MethodName, cSharpClasses);
                var type = assembly.GetType(fullName);
                //AppDomain not supported still for now restart app
                //AppDomain.Unload();
                return type;
            }
        }

        private void ValidateCodeSize(string[] cSharpClasses)
        {
            int bytes = 0;

            foreach(var cSharpClass in cSharpClasses)
            {
                bytes += System.Text.ASCIIEncoding.Unicode.GetByteCount(cSharpClass);
            }

            if(bytes >= Configuration.CodeBlockByteLimit)
            {
                throw new TeacherException($"Classes combined was above 100KB.");
            }
        }
    }
}
