﻿using System;
using System.Collections.Generic;

namespace LearnSoftware.ViewModels.ForumCategories
{
    public class ForumCategoryIndexContainerViewModel
    {
        public ForumCategoryIndexViewModel ForumCategoryViewModel { get; set; }

        public List<ForumCategoryViewModel> ForumCategories { get; set; }
    }
}
