﻿using System;
using System.Collections.Generic;
using System.Linq;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.ViewModels.Exercises;
using LearnSoftware.Controllers;
using LearnSoftware.Models;
using LearnSoftware.Models.Learn;
using LearnSoftware.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LearnSoftware.Services.Interfaces;
using LearnSoftware.Services.Dtos;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LearnSoftware.Areas.Learn.Controllers
{
    [Area("Learn")]
    public class ExercisesController : Controller
    {
        private const string CreateTestsSessionKey = "CreateTests";
        private const string EditTestsSessionKey = "EditTests";

        private IExerciseService exerciseService;
        private ILectureService lectureService;
        private ISessionManager sessionManager;
        private ISubscriptionService subManager;
        private UserManager<LSIdentityUser> userManager;
        private ILoadBalancingService loadBalancingService;
        private IConfiguration configuration;
        private IPageManager pageService;

        public ExercisesController(
            IExerciseService exerciseService, 
            ILectureService lectureService, 
            ISessionManager sessionManager, 
            ISubscriptionService subManager, 
            UserManager<LSIdentityUser> userManager, 
            ILoadBalancingService loadBalancingService,
            IConfiguration configuration,
            IPageManager pageService)
        {
            this.exerciseService = exerciseService;
            this.lectureService = lectureService;
            this.sessionManager = sessionManager;
            this.subManager = subManager;
            this.userManager = userManager;
            this.loadBalancingService = loadBalancingService;
            this.configuration = configuration;
            this.pageService = pageService;
        }

        [Authorize]
        public IActionResult Exercise(int id, string error, int page = 1)
        {
            if(!String.IsNullOrEmpty(error))
            {
                ModelState.AddModelError("Submission", error);
            }

            var exercise = exerciseService.GetExercise((int)id);

            if (exercise == null)
            {
                return this.NotFound();
            }

            if (!exercise.IsCourseActive && !this.User.IsInRole("Admin"))
            {
                return this.Unauthorized();
            }

            ViewData["isUserSubscribed"] = subManager.IsUserSubscribed(this.User.Identity.Name, exercise.Lecture.CourseId);

            IEnumerable<TestResult> results = exerciseService.GetTestResults(exercise, this.User.Identity.Name, page);
            var exerciseTestsCount = exercise.ExerciseTests.Count();
            var maxPage = pageService.GetMaxPage(exerciseService.GetTestResultsCount(exercise, this.User.Identity.Name) / exerciseTestsCount);
            var minPage = pageService.GetMinPage(maxPage);

            var model = new ExerciseExerciseViewModel()
            {
                Id = exercise.Id,
                Name = exercise.Name,
                CourseType = exercise.Lecture?.Course?.CourseType ?? 0,
                Description = exercise.Description,
                ExampleInput1 = exercise.ExampleInput1,
                ExampleOutput1 = exercise.ExampleOutput1,
                ExampleInput2 = exercise.ExampleInput2,
                ExampleOutput2 = exercise.ExampleOutput2,
                ExerciseTestCount = exerciseTestsCount,
                ExerciseTestResults = results.Select( tr => new ExerciseTestResultViewModel()
                {
                    Id = tr.Id,
                    Result = tr.Success == null ? "Not Ran" : tr.Success == true ? "Pass" : "Fail",
                    Message = tr.Message,
                    TestResultLines = tr.TestResultLines.Select(trl => new ExerciseTestResultLineViewModel() 
                    { 
                        Number = trl.LineNumber, 
                        Match = trl.Match, 
                        ExpectedResult = trl.ExpectedResult, 
                        ActualResult = trl.ActualResult 
                    }).ToList()
                }).ToList(),
                MinPage = minPage,
                MaxPage = maxPage,
            };

            return this.View(model);
        }



        [Authorize]
        public IActionResult Refresh(int exerciseId)
        {
            var exercise = exerciseService.GetExercise((int)exerciseId);

            if (exercise == null)
            {
                return this.NotFound();
            }

            if (!exercise.IsCourseActive && !this.User.IsInRole("Admin"))
            {
                return this.Unauthorized();
            }

            ViewData["isUserSubscribed"] = subManager.IsUserSubscribed(this.User.Identity.Name, exercise.Lecture.CourseId);

            IEnumerable<TestResult> results = exerciseService.GetTestResults(exercise, this.User.Identity.Name, 1);

            var maxPage = pageService.GetMaxPage(exerciseService.GetTestResultsCount(exercise, this.User.Identity.Name));
            var minPage = pageService.GetMinPage(maxPage);

            var model = new ExerciseExerciseViewModel() 
            { 
                ExerciseTestCount = exercise.ExerciseTests.Count(),
                MinPage = minPage,
                MaxPage = maxPage,
            };

            model.ExerciseTestResults = results.Select(tr => new ExerciseTestResultViewModel()
            {
                Id = tr.Id,
                Result = tr.Success == null ? "Not Ran" : tr.Success == true ? "Pass" : "Fail",
                Message = tr.Message,
                TestResultLines = tr.TestResultLines.Select(trl => new ExerciseTestResultLineViewModel()
                {
                    Number = trl.LineNumber,
                    Match = trl.Match,
                    ExpectedResult = trl.ExpectedResult,
                    ActualResult = trl.ActualResult
                }).ToList()
            }).ToList();

            return this.PartialView("_TestResultsPartial", model);
        }

        [HttpPost]
        [Authorize]
        public IActionResult SubmitCode(ExerciseExerciseViewModel model)
        {
            if(!this.ModelState.IsValid)
            {
                return RedirectToAction("Exercise", "Exercises", new { id = model.Id, error = "Submission needs more than 20 chars." });
            }

            Exercise exercise = exerciseService.GetExercise(model.Id);

            if (model.CourseType != CourseType.Free && !subManager.IsUserSubscribed(this.User.Identity.Name, exercise.Lecture.CourseId) && !this.User.IsInRole("Admin"))
            {
                return this.Unauthorized();
            }

            var user = userManager.FindByNameAsync(this.User.Identity.Name).Result;

            if(exercise == null || user == null)
            {
                return this.NotFound();
            }

            if (!exercise.IsCourseActive && !this.User.IsInRole("Admin"))
            {
                return this.Unauthorized();
            }

            exerciseService.SubmitCode(model.Code, exercise, user);

            return RedirectToAction("Exercise", "Exercises", new { id = model.Id });
        }

        [HttpGet("/Exercises/Edit/{exerciseId}")]
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(int exerciseId)
        {
            var exercise = exerciseService.GetExercise(exerciseId);
            var exerciseTests = exerciseService.GetExerciseTestsWithIO(exercise);

            if (exercise == null)
            {
                return this.NotFound();
            }

            ExerciseCreateViewModel model = new ExerciseCreateViewModel()
            {
                Id = exercise.Id,
                Description = exercise.Description,
                ExampleInput1 = exercise.ExampleInput1,
                ExampleInput2 = exercise.ExampleInput2,
                ExampleOutput1 = exercise.ExampleOutput1,
                ExampleOutput2 = exercise.ExampleOutput2,
                Name = exercise.Name,
                TestLines = exerciseTests
                    .Select(e => new TestLinesViewModel() 
                    {
                        TestInputLines = String.Join(Environment.NewLine, e.TestInputs.Select(t => t.Text)), 
                        TestExpectedOutputLines = e.TestExpectedOutputLines
                    }).ToList()
            };

            this.sessionManager.SetSessionObject(EditTestsSessionKey, model.TestLines, this.HttpContext.Session);

            return View(model);
        }

        [HttpPost("/Exercises/Edit/{exerciseId}")]
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(ExerciseCreateViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            if (model.TestLines.Count == 0)
            {
                ModelState.AddModelError("Tests", "Atleast 1 Test is needed.");
                return this.View(model);
            }

            var exercise = exerciseService.GetExercise(model.Id);

            if (exercise == null)
            {
                return this.NotFound();
            }

            exerciseService.UpdateExercise(
                exercise, model.Name, model.Description, model.ExampleInput1, model.ExampleOutput1,
                model.ExampleInput2, model.ExampleOutput2, model.TestLines);

            return RedirectToAction("Exercise", "Exercises", new { model.Id });
        }

        // GET: /<controller>/
        [Authorize(Roles = "Admin")]
        public IActionResult Create(int lectureId)
        {
            if (!lectureService.HasLecture(lectureId))
            {
                return this.NotFound();
            }

            ExerciseCreateViewModel model = new ExerciseCreateViewModel()
            {
                LectureId = (int)lectureId,
                TestLines = this.sessionManager
                                    .GetSessionObject<List<TestLinesViewModel>>(CreateTestsSessionKey, this.HttpContext.Session) ?? new List<TestLinesViewModel>()
            };

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult Create(ExerciseCreateViewModel model)
        {
            if (!this.ModelState.IsValid)
            {               
                return this.View(model);
            }

            if(model.TestLines.Count == 0)
            {
                ModelState.AddModelError("Tests", "Atleast 1 Test is needed.");
                return this.View(model);
            }

            var lecture = lectureService.GetLecture(model.LectureId);

            if (lecture == null)
            {
                return this.NotFound();
            }

            int exerciseId = exerciseService.CreateExercise(
                lecture, model.Name, model.Description, model.ExampleInput1 ?? String.Empty, 
                model.ExampleOutput1, model.ExampleInput2, model.ExampleOutput2,  
                model.TestLines.Select(tl => 
                new TestLinesViewModel() { 
                    TestInputLines = tl.TestInputLines ?? String.Empty,
                    TestExpectedOutputLines = tl.TestExpectedOutputLines 
                }).ToList());

            this.sessionManager.SetSessionObject(EditTestsSessionKey, new List<TestLinesViewModel>(), this.HttpContext.Session);

            return RedirectToAction("Exercise", "Exercises", new { id = exerciseId });
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult AddTest(ExerciseCreateViewModel model)
        {
            var tests = this.sessionManager
                            .GetSessionObject<List<TestLinesViewModel>>(CreateTestsSessionKey, this.HttpContext.Session) ?? new List<TestLinesViewModel>();

            if (String.IsNullOrEmpty(model.CurrentTestLines?.TestExpectedOutputLines))
            {
                ModelState.AddModelError("Tests", "Please fill expected output lines.");
                model.TestLines = tests;
                return this.View("Create", model);
            }

            if (String.IsNullOrEmpty(model.CurrentTestLines.TestInputLines))
            {
                model.CurrentTestLines.TestInputLines = String.Empty;
            }

            tests.Add(model.CurrentTestLines);
            this.sessionManager.SetSessionObject(CreateTestsSessionKey, tests, this.HttpContext.Session);

            model.TestLines = tests;

            return this.View("Create", model);
        }

        [HttpPost("/Exercises/RemoveTest/{index}")]
        [Authorize(Roles = "Admin")]
        public IActionResult RemoveTest(ExerciseCreateViewModel model, int? index)
        {
            if (index == null)
            {
                return this.View("Create", model);
            }

            var tests = this.sessionManager
                            .GetSessionObject<List<TestLinesViewModel>>(CreateTestsSessionKey, this.HttpContext.Session) ?? new List<TestLinesViewModel>();

            if (index >= tests.Count)
            {
                return this.View("Create", model);
            }

            tests.RemoveAt((int)index);

            this.sessionManager.SetSessionObject(CreateTestsSessionKey, tests, this.HttpContext.Session);
            model.TestLines = tests;

            return this.View("Create", model);
        }
    }
}
