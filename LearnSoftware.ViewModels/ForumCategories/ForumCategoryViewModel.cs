﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearnSoftware.ViewModels.ForumCategories
{
    public class ForumCategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
