﻿using System;
using System.Linq;
using LearnSoftware.Areas.Identity.Data;
using LearnSoftware.ViewModels.Forums;
using LearnSoftware.ViewModels.Messges;
using LearnSoftware.Controllers;
using LearnSoftware.Models;
using LearnSoftware.Models.Information;
using LearnSoftware.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LearnSoftware.Services.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LearnSoftware.Areas.Information.Controllers
{
    [Area("Information")]
    public class ForumMessagesController : Controller
    {
        private IForumMessageService forumMessageService;
        private IForumService forumService;
        private UserManager<LSIdentityUser> userManager;

        public ForumMessagesController(IForumMessageService forumMessageService, IForumService forumService, UserManager<LSIdentityUser> userManager)
        {
            this.forumMessageService = forumMessageService;
            this.forumService = forumService;
            this.userManager = userManager;
        }

        [Authorize]
        //[HttpGet("/ForumMessages/Edit/{forumMessageId}")]
        public IActionResult Edit(int forumMessageId, int page = 1)
        {
            var message = this.forumMessageService.GetForumMessage((int)forumMessageId);

            if(message == null)
            {
                return this.NotFound();
            }

            if(this.User.Identity.Name != message.User.UserName || !message.Forum.IsActive)
            {
                return this.Unauthorized();
            }

            var model = new MessageViewModel()
            {
                Id = message.Id,
                Message = message.Message,
                CurrentPage = page
            };

            return this.View(model);
        }

        [HttpPost]
        [Authorize]
        //[HttpPost("/ForumMessages/Edit/{forumMessageId}")]
        public IActionResult Edit(MessageViewModel model)
        {
            if(!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var message = this.forumMessageService.GetForumMessage(model.Id);

            if (message == null)
            {
                return this.NotFound();
            }

            if (this.User.Identity.Name != message.User.UserName || !message.Forum.IsActive)
            {
                return this.Unauthorized();
            }

            forumMessageService.UpdateMessage(message, model.Message);

            return this.RedirectToAction("Forum", "Forums", new { id = message.Forum.Id, page = model.CurrentPage });
        }

        [Authorize]
        [HttpPost]
        public IActionResult Create(ForumViewModel model)
        {
            if(!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var forum = forumService.GetForum(model.Id);
            var user = userManager.FindByNameAsync(this.User.Identity.Name).Result;

            if (forum == null || user == null)
            {
                return this.NotFound();
            }

            if(!forum.IsActive)
            {
                return this.Unauthorized();
            }

            forumMessageService.AddMessage(forum, user, model.NewComment);

            return this.RedirectToAction("Forum", "Forums", new { id = forum.Id, page = model.MaxPage });
        }
    }
}
