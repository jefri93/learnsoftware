﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LearnSoftware.ViewModels.Courses
{
    public class CoursesIndexContainerViewModel
    {
        public CoursesIndexContainerViewModel()
        {
            Courses = new List<CourseIndexViewModel>();
        }

        public List<CourseIndexViewModel> Courses { get; set; }

        public bool InProgress { get; set; }

        public bool Purchased { get; set; }
    }
}
